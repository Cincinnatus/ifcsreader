﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace IFCSReader
{

    class IfcsGenerator
    {
        //
        //Acceleration = Jmax  x (TimeGoal - sqrt(TimeGoal^2 - 4 x (1/jerk) * (-Vmax))
        //
        internal const string Build = "483389";
        internal const bool isPostABCruise = true;
        internal const Boolean IsOutputOn = false;
        internal const bool isLastMassOutputOn = false;
        internal const bool getTunedMass = false;
        internal const bool graphDistance = false;
        internal string[] ReportKeys = new string[] { "Velocity Goal", "Velocity@Goal", "Variance" };
        internal const string REPORT_NAME = "IfcsReport";
        internal const string IFCS_EXPORT_DATA = "IfcsData";
        //Get directory path
        internal static string dirPath = @"D:\Temp\" + Build + @"\Scripts\Entities\Vehicles\Implementations\Xml";
        internal string modPath = dirPath + @"\Modifications";
        internal static bool BOOSTED = false;


        static void Main(string[] args)
        {
            List<string> files = new List<string>();
            List<string> mods = new List<string>();

            try
            {


                //get list of files
                files = new List<string>(Directory.EnumerateFiles(dirPath));

                //echo each file
                foreach (var file in files)
                {
                    Console.WriteLine("{0}", file.Substring(file.LastIndexOf("\\") + 1));
                }
                Console.WriteLine("{0} files found.", files.Count);
            }
            catch (UnauthorizedAccessException uaEx)
            {
                Console.WriteLine(uaEx.Message);
            }
            catch (PathTooLongException pathEx)
            {
                Console.WriteLine(pathEx.Message);
            }

            Dictionary<string, Spaceship> ships = new Dictionary<string, Spaceship>();

            GetShips(files, ships);
            GetVariants(ships);

            if (BOOSTED)
            {
                foreach (var s in ships)
                {
                    s.Value.GetBoosted();
                }
            }


            //IFCSReporter.GetTunedMass(ships);
            IFCSReporter.GetMotion(ships);
            //IFCSReporter.ExportMotion(ships);
            IFCSReporter.GetReport(ships);
            IFCSReporter.ExportReport(ships);
            IFCSReporter.ExportIFCSData(ships);
            IFCSReporter.ExportSQL(ships);

            //Test stuff
            GetGraphs(ships);



            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void GetGraphs(Dictionary<string, Spaceship> ships)
        {
            foreach (var k in ships)
            {
                var motions = k.Value.MotionsDictionary;
                foreach (var m in motions)
                {
                    MyGraph.GetGraph(@"D:\Temp\" + Build + @"\" + Build + "-" + k.Value.Localname + "-" + m.Key + ".png",
                        m.Value, Build + "-" + k.Value.Localname + "-" + m.Key);
                }
            }
        }

        private static void GetVariants(Dictionary<string, Spaceship> ships)
        {
            Dictionary<string, Spaceship> variants = new Dictionary<string, Spaceship>();
            foreach (var item in ships)
            {
                foreach (var variant in item.Value.Variants)
                {
                    variants.Add(variant.Localname,variant);
                }
            }

            foreach (var variant in variants)
            {
                ships.Add(variant.Key,variant.Value);
            }
        }

        private static void GetShips(List<string> files, Dictionary<string, Spaceship> ships)
        {
            foreach (var file in files)

            {
                var jdata = GetJson(file);

                Spaceship ship = new Spaceship(file);
                ship.LoadJson(jdata, false);

                ships.Add(ship.Localname, ship);
            }
        }

        public static dynamic GetJson(string file)
        {
            var doc = new XmlDocument();
            doc.Load(file);

            string json = JsonConvert.SerializeXmlNode(doc);
            json = json.Replace("@", "");

            dynamic jdata = JObject.Parse(json);
            return jdata;
        }

        public static dynamic GetJson(XmlDocument doc)
        {
            string json = JsonConvert.SerializeXmlNode(doc);
            json = json.Replace("@", "");

            dynamic jdata = JObject.Parse(json);
            return jdata;
        }

        public static XDocument getXDocument(string file)
        {
            string xml = File.ReadAllText(file);
            var doc = XDocument.Load(xml);
            return doc;
        }




    }
}