﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace IFCSReader
{
    internal class Utilities
    {
        public static class DocumentExtensions
        {
            public static XmlDocument ToXmlDocument(XDocument xDocument)
            {
                var xmlDocument = new XmlDocument();
                using (var xmlReader = xDocument.CreateReader())
                {
                    xmlDocument.Load(xmlReader);
                }
                return xmlDocument;
            }

            public static XDocument ToXDocument(XmlDocument xmlDocument)
            {
                using (var nodeReader = new XmlNodeReader(xmlDocument))
                {
                    nodeReader.MoveToContent();
                    return XDocument.Load(nodeReader);
                }
            }

            public static XDocument mergeDocuments(XDocument baseFile, XDocument modFile)
            {
                //step one find all nodes with the value ID in the mod file;
                var modElements = modFile.XPathSelectElements("//*[@id]");
                var baseElements = modFile.XPathSelectElements("//*[@id]");

                foreach (var m in modElements)
                {
                    var selected = baseFile.XPathSelectElement("//" + m.Name);
                    try
                    {
                        baseFile.XPathSelectElement("//" + m.Name).ReplaceWith(m);
                        Console.WriteLine("Patching: " + selected.Name);
                    }
                    catch
                    {
                        Console.WriteLine("Unable to patch: " + m.Name.ToString());
                    }
                    
                    
                }


                return baseFile;

            }
        }
    }
}
