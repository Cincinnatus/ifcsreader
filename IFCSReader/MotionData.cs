﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFCSReader
{
    public struct MotionData
    {
        public double Time, Distance, Velocity, Acceleration, Jerk;

        public MotionData(double time, double distance, double velocity, double acceleration, double jerk)
        {
            this.Time = time;
            this.Distance = distance;
            this.Velocity = velocity;
            this.Acceleration = acceleration;
            this.Jerk = jerk;
        }
    }
}
