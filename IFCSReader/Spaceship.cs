﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;

using System.Xml.Linq;
using System.Xml.XPath;

using Newtonsoft.Json.Linq;

namespace IFCSReader
{
    internal class Spaceship
    {
        public string fileName = "";
        bool hasVariants = false;
        bool isVariant = false;
        public List<Spaceship> Variants = new List<Spaceship>();
        public Ifcs Ifcs { get; set; } = new Ifcs();
        public string Localname { get; set; } = "";
        public string Shipname { get; set; } = "";
        public string Shipbase { get; set; } = "";
        public string Shipsize { get; set; } = "";

        //The profiles
        public Dictionary<string, List<MotionData>> MotionsDictionary = new Dictionary<string, List<MotionData>>();
        public Dictionary<string, string> ProfileReport = new Dictionary<string, string>();
        public Dictionary<string,string> PatchList = new Dictionary<string,string>();

        public Spaceship(string fileName)
        {
            this.fileName = fileName;
        }

        public Spaceship(string fileName, bool isVariant)
        {
            List<Spaceship> Variants = new List<Spaceship>();
            this.hasVariants = false;
            this.isVariant = isVariant;
        }


        public void AddReport(Report report)
        {
            this._privateReports.Add(report);
        }

        private List<Report> _privateReports = new List<Report>();

        public List<Report> GetReports()
        {
            return _privateReports;
        }

        public void LoadJson(dynamic data, bool isVariant)
        {

            if (isVariant)
            {
                //dont apply name, already set.
            }
            else if (data.displayname != null)
            {
                Localname = data.Vehicle.displayname;
            } 
            else
            {
                Localname = data.Vehicle.name;
                Shipbase = "";
                Shipsize = data.Vehicle.size;
            }

            dynamic fcd;

            if (data.Vehicle.MovementParams.Spaceship.IFCS.states != null)
            {
                fcd = data.Vehicle.MovementParams.Spaceship.IFCS.states.state[0];
            }
            else
            {
                fcd = data.Vehicle.MovementParams.Spaceship.IFCS;
            }


            //Tuning
            Ifcs.Tuning.ScmVelocity = fcd.tuning.SCMVelocity;
            Ifcs.Tuning.AbmVelocity = fcd.tuning.ABMVelocity;
            Ifcs.Tuning.CmVelocity = 0f;

            //TunedParams
            //mass
            Ifcs.Tuned.Mass.Hull = fcd.TunedParams.mass.hull ?? 0f;
            Ifcs.Tuned.Mass.Items = fcd.TunedParams.mass.items ?? 0f;
            Ifcs.Tuned.Mass.Total = fcd.TunedParams.mass.total ?? 0f;
            //LinearThrust
            Ifcs.Tuned.LinearThrust.Pos = new Xyz(fcd.TunedParams.linearThrust.pos.ToString());
            Ifcs.Tuned.LinearThrust.Neg = new Xyz(fcd.TunedParams.linearThrust.neg.ToString());
            //LinearScale
            Ifcs.Tuned.LinearScale.Pos = new Xyz(fcd.TunedParams.linearScale.pos.ToString());
            Ifcs.Tuned.LinearScale.Neg = new Xyz(fcd.TunedParams.linearScale.neg.ToString());
            //LinearJerk
            Ifcs.Tuned.LinearJerk.Pos = new Xyz(fcd.TunedParams.linearJerk.pos.ToString());
            Ifcs.Tuned.LinearJerk.Neg = new Xyz(fcd.TunedParams.linearJerk.neg.ToString());
            //LinearJerkScale
            Ifcs.Tuned.LinearJerkScale.Pos = new Xyz(fcd.linearJerk.posScale.ToString());
            Ifcs.Tuned.LinearJerkScale.Neg = new Xyz(fcd.linearJerk.negScale.ToString());
            //Angular Velocity
            Ifcs.Tuning.AngularVelocity = new Xyz(fcd.tuning.AngVelocity.ToString());
            //AngularThrust
            Ifcs.Tuned.AngularThrust.Pos = new Xyz(fcd.TunedParams.angularThrust.pos.ToString());
            Ifcs.Tuned.AngularThrust.Neg = new Xyz(fcd.TunedParams.angularThrust.neg.ToString());
            //AngularScale
            Ifcs.Tuned.AngularScale.Pos = new Xyz(fcd.TunedParams.angularScale.pos.ToString());
            Ifcs.Tuned.AngularScale.Neg = new Xyz(fcd.TunedParams.angularScale.neg.ToString());
            //AngularJerk
            Ifcs.Tuned.AngularJerk.Value = new Xyz(fcd.TunedParams.angularJerk.value.ToString());
            //AngularJerkScale
            Ifcs.Tuned.AngularJerkScale.Value = new Xyz(fcd.angularJerk.scale.ToString());
            //GoalstopTimes
            Ifcs.GoalStopTimes.Linear.Pos = new Xyz(fcd.goalStopTimes.linPos.ToString());
            Ifcs.GoalStopTimes.Linear.Neg = new Xyz(fcd.goalStopTimes.linNeg.ToString());
            Ifcs.GoalStopTimes.Angular.Value = new Xyz(fcd.goalStopTimes.ang.ToString());
            //AngularJerk
            Ifcs.Tuned.RotationScale = fcd.TunedParams.rotationScale.value;
            Ifcs.Tuned.CruiseTime = fcd.TunedParams.cruiseTime.value;
            Ifcs.Tuned.Boost = fcd.tuning.boostScale ?? 1f;
            Ifcs.Tuned.ABBoost = fcd.tuning.ABScale ?? 1f;




            //Get aggregate thrust
            dynamic mounts = fcd.TunedParams.shunting.mount;
            foreach (var mount in  mounts)
            {
                Ifcs.Tuned.AggregateThrust+= float.Parse(mount.capacity.ToString());
            }

            //Get modifications
            try
            {
                if (this.Localname.Contains("Constellation") || this.Localname.Contains("Freelancer"))
                {
                    Console.WriteLine("Does not have base real modifications...yet");
                }
               
                foreach (dynamic mod in data.Vehicle.Modifications.Modification)
                {
                    string patchFile = "";
                    string name = "";
                    try
                    {
                        patchFile = mod.patchFile;
                        patchFile = patchFile.Replace("/", "\\");
                       
                        name = mod.name;
                        if (name.Contains("Dead") || name.Contains("F7A") || name.Contains("SQ42") || patchFile.Contains("Constellation"))
                        {
                            Console.WriteLine("Invalid patchfile");
                            continue;
                        }

                    }
                    catch
                    {
                        Console.WriteLine("Invalid patchfile");
                        continue;
                    }

                    this.PatchList.Add(name,patchFile);
                    Console.WriteLine("Added patchfile.");



                }
            }
            catch
            {
                
            }

            if (this.PatchList.Count != 0)
            {
                //generate merged XML file

                foreach (var patch in this.PatchList)
                {
                    var baseFile = XDocument.Load(this.fileName);
                    var f = IfcsGenerator.dirPath + "\\" + patch.Value + ".xml";
                    if (File.Exists(f))
                    {
                        var modFile = XDocument.Load(f);

                        XDocument merged = Utilities.DocumentExtensions.mergeDocuments(baseFile, modFile);
                        var z = merged.XPathSelectElement("//Modifications");
                        merged.XPathSelectElement("//Modifications").Remove();

                        var DynamicData = IfcsGenerator.GetJson(Utilities.DocumentExtensions.ToXmlDocument(baseFile));

                        var s = new Spaceship(f, true);
                        s.Localname = patch.Key;
                        s.Shipbase = this.Localname;
                        s.LoadJson(DynamicData, true);
                        this.Variants.Add(s);
                    }

                }
            }
        }

        public void GetBoosted()
        {
            //Boost jerk
            this.Ifcs.Tuned.LinearJerk.Pos.X *= this.Ifcs.Tuned.Boost;
            this.Ifcs.Tuned.LinearJerk.Pos.Y *= this.Ifcs.Tuned.Boost;
            this.Ifcs.Tuned.LinearJerk.Pos.Z *= this.Ifcs.Tuned.Boost;

            this.Ifcs.Tuned.LinearJerk.Neg.X *= this.Ifcs.Tuned.Boost;
            this.Ifcs.Tuned.LinearJerk.Neg.Y *= this.Ifcs.Tuned.Boost;
            this.Ifcs.Tuned.LinearJerk.Neg.Z *= this.Ifcs.Tuned.Boost;

            //Boost thrust
            this.Ifcs.Tuned.LinearThrust.Pos.X *= this.Ifcs.Tuned.Boost;
            this.Ifcs.Tuned.LinearThrust.Pos.Y *= this.Ifcs.Tuned.Boost;
            this.Ifcs.Tuned.LinearThrust.Pos.Z *= this.Ifcs.Tuned.Boost;

            this.Ifcs.Tuned.LinearThrust.Neg.X *= this.Ifcs.Tuned.Boost;
            this.Ifcs.Tuned.LinearThrust.Neg.Y *= this.Ifcs.Tuned.Boost;
            this.Ifcs.Tuned.LinearThrust.Neg.Z *= this.Ifcs.Tuned.Boost;

            this.Ifcs.GoalStopTimes.Linear.Pos.X /= this.Ifcs.Tuned.Boost;
            this.Ifcs.GoalStopTimes.Linear.Pos.Y /= this.Ifcs.Tuned.Boost;
            this.Ifcs.GoalStopTimes.Linear.Pos.Z /= this.Ifcs.Tuned.Boost;

            this.Ifcs.GoalStopTimes.Linear.Neg.X /= this.Ifcs.Tuned.Boost;
            this.Ifcs.GoalStopTimes.Linear.Neg.Y /= this.Ifcs.Tuned.Boost;
            this.Ifcs.GoalStopTimes.Linear.Neg.Z /= this.Ifcs.Tuned.Boost;
        }

        public DoubleXyz GetBoostedLinearScmGoalTimes()
        {
            DoubleXyz total = new DoubleXyz(
            //pos
            this.Ifcs.Tuning.ScmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Pos.X),
            this.Ifcs.Tuning.ScmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.Y) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Pos.Y),
            this.Ifcs.Tuning.ScmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.Z) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Pos.Z),
            //neg
            this.Ifcs.Tuning.ScmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Neg.X) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Neg.X),
            this.Ifcs.Tuning.ScmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Neg.Y) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Neg.Y),
            this.Ifcs.Tuning.ScmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Neg.Z) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Neg.Z));
            return total;
        }

        public DoubleXyz GetBoostedLinearAbmGoalTimes()
        {
            DoubleXyz total = new DoubleXyz(
            //pos
            this.Ifcs.Tuning.AbmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Pos.X),
            this.Ifcs.Tuning.AbmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.Y) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Pos.Y),
            this.Ifcs.Tuning.AbmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.Z) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Pos.Z),
            //neg
            this.Ifcs.Tuning.AbmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Neg.X) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Neg.X),
            this.Ifcs.Tuning.AbmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Neg.Y) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Neg.Y),
            this.Ifcs.Tuning.AbmVelocity / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Neg.Z) + (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X) / (this.Ifcs.Tuned.Boost * this.Ifcs.Tuned.LinearJerk.Neg.Z));
            return total;
        }

        public DoubleXyz GetBoostedLinearAbmStopDistance()
        {
            DoubleXyz total = new DoubleXyz(
            //pos
            GetTotalDistance(this.Ifcs.GoalStopTimes.Linear.Neg.X, this.Ifcs.Tuning.AbmVelocity, this.Ifcs.Tuned.LinearAccelerationTotal().Pos.X, this.Ifcs.Tuned.LinearJerk.Pos.X, this.Ifcs.Tuned.Boost),
            GetTotalDistance(this.Ifcs.GoalStopTimes.Linear.Neg.Y, this.Ifcs.Tuning.AbmVelocity, this.Ifcs.Tuned.LinearAccelerationTotal().Pos.Y, this.Ifcs.Tuned.LinearJerk.Pos.Y, this.Ifcs.Tuned.Boost),
            GetTotalDistance(this.Ifcs.GoalStopTimes.Linear.Neg.Z, this.Ifcs.Tuning.AbmVelocity, this.Ifcs.Tuned.LinearAccelerationTotal().Pos.Z, this.Ifcs.Tuned.LinearJerk.Pos.Z, this.Ifcs.Tuned.Boost),
            //neg
            GetTotalDistance(this.Ifcs.GoalStopTimes.Linear.Neg.X, this.Ifcs.Tuning.AbmVelocity, this.Ifcs.Tuned.LinearAccelerationTotal().Neg.X, this.Ifcs.Tuned.LinearJerk.Neg.X, this.Ifcs.Tuned.Boost),
            GetTotalDistance(this.Ifcs.GoalStopTimes.Linear.Neg.Y, this.Ifcs.Tuning.AbmVelocity, this.Ifcs.Tuned.LinearAccelerationTotal().Neg.Y, this.Ifcs.Tuned.LinearJerk.Neg.Y, this.Ifcs.Tuned.Boost),
            GetTotalDistance(this.Ifcs.GoalStopTimes.Linear.Neg.Z, this.Ifcs.Tuning.AbmVelocity, this.Ifcs.Tuned.LinearAccelerationTotal().Neg.Z, this.Ifcs.Tuned.LinearJerk.Neg.Z, this.Ifcs.Tuned.Boost)
            );
            return total;
        }

        private float GetTotalDistance(float t, float abmVelocity, float a, float j, float boost)
        {
            float t1 = a/j;
            float t2 = abmVelocity/a - t1;
            float totalDistance = 0f;
            MotionData lastMotionData;

            //acceleration phase
            lastMotionData = GetDistance(new MotionData(t1, 0d, 0d, 0d,j));
            lastMotionData = GetDistance(new MotionData(t2, lastMotionData.Distance, lastMotionData.Velocity, a, 0f));
            lastMotionData = GetDistance(new MotionData(t1, lastMotionData.Distance, lastMotionData.Velocity, a, -j));

            //cruising phase

            //deceleration phase
            return (float) lastMotionData.Distance;
        }

        private MotionData GetDistance(MotionData data)
        {
            double x = data.Distance
                               + data.Velocity * data.Time
                               + 0.5f * data.Acceleration * data.Time * data.Time
                               + (1 / 6f) * data.Jerk * data.Time * data.Time * data.Time;

            double v = data.Velocity * data.Time
                   + 0.5f * data.Acceleration * data.Time * data.Time
                   + (1 / 6f) * data.Jerk * data.Time * data.Time * data.Time;

            return new MotionData(0d,x,v,0d,0d);

        }

        
    }

    internal struct Report
    {
        internal string AxisLabel;
        internal double VelocityAtGoal;
        internal double Variance;

        internal Report(string axisLabel, double velocityAtGoal, double variance)
        {
            this.AxisLabel = axisLabel;
            this.VelocityAtGoal = velocityAtGoal;
            this.Variance = variance;
        }
    }

    internal class Ifcs
    {
        public Tuned Tuned { get; set; }
        public Tuning Tuning { get; set; }
        public GoalStopTimes GoalStopTimes { get; set; }

        public Ifcs()
        {
            this.Tuned = new Tuned();
            this.Tuning = new Tuning();
            this.GoalStopTimes = new GoalStopTimes();
        }
    }

    internal class Tuning
    {
        public int ThrusterCount { get; set; }
        public float ScmVelocity { get; set; }
        public float AbmVelocity { get; set; }
        public float CmVelocity { get; set; }
        public Xyz AngularVelocity { get; set; }
    }

    internal class GoalStopTimes
    {
        public GoalStopTimes()
        {
            this.Linear = new DoubleXyz();
            this.Angular = new SingleXyz();
        }

        public DoubleXyz Linear { get; set; }
        public SingleXyz Angular { get; set; }

        public DoubleXyz GetMainsRatio()
        {
            DoubleXyz total = new DoubleXyz(
            //pos
            this.Linear.Pos.X / this.Linear.Neg.Y,
            this.Linear.Pos.Y / this.Linear.Neg.Y,
            this.Linear.Pos.Z / this.Linear.Neg.Y,
            //neg
             this.Linear.Neg.X / this.Linear.Neg.Y,
             this.Linear.Neg.Y / this.Linear.Neg.Y,
            this.Linear.Neg.Z / this.Linear.Neg.Y);
            return total;
        }

    }

    internal class Tuned
    {
        public DoubleXyz LinearThrustTotal()
        {
            DoubleXyz total = new DoubleXyz(
                //pos
                this.LinearThrust.Pos.X * this.LinearScale.Pos.X,
                this.LinearThrust.Pos.Y * this.LinearScale.Pos.Y,
                this.LinearThrust.Pos.Z * this.LinearScale.Pos.Z,
                //neg
                this.LinearThrust.Neg.X * this.LinearScale.Neg.X,
                this.LinearThrust.Neg.Y * this.LinearScale.Neg.Y,
                this.LinearThrust.Neg.Z * this.LinearScale.Neg.Z);

            ////pos
            //this.LinearThrust.Pos.X * 1f,
            //this.LinearThrust.Pos.Y * 1f,
            //this.LinearThrust.Pos.Z * 1f,
            ////neg
            //this.LinearThrust.Neg.X * 1f,
            //this.LinearThrust.Neg.Y * 1f,
            //this.LinearThrust.Neg.Z * 1f);

            return total;
        }
        public DoubleXyz LinearJerkTotal()
        {
            DoubleXyz total = new DoubleXyz(
                //pos
                this.LinearJerk.Pos.X*this.LinearJerkScale.Pos.X,
                this.LinearJerk.Pos.Y*this.LinearJerkScale.Pos.Y,
                this.LinearJerk.Pos.Z*this.LinearJerkScale.Pos.Z,
                //neg
                this.LinearJerk.Neg.X*this.LinearJerkScale.Neg.X,
                this.LinearJerk.Neg.Y*this.LinearJerkScale.Neg.Y,
                this.LinearJerk.Neg.Z*this.LinearJerkScale.Neg.Z);
            return total;
        }
        public DoubleXyz AngularThrustTotal()
        {
            DoubleXyz total = new DoubleXyz(
                //pos
                this.AngularThrust.Pos.X*this.AngularScale.Pos.X,
                this.AngularThrust.Pos.Y*this.AngularScale.Pos.Y,
                this.AngularThrust.Pos.Z*this.AngularScale.Pos.Z,
                //neg
                this.AngularThrust.Neg.X*this.AngularScale.Neg.X,
                this.AngularThrust.Neg.Y*this.AngularScale.Neg.Y,
                this.AngularThrust.Neg.Z*this.AngularScale.Neg.Z);
            return total;
        }
        public SingleXyz AngularJerkTotal()
        {
            SingleXyz total = new SingleXyz(
                //value
                this.AngularJerk.Value.X*this.AngularJerkScale.Value.X,
                this.AngularJerk.Value.Y*this.AngularJerkScale.Value.Y,
                this.AngularJerk.Value.Z*this.AngularJerkScale.Value.Z);
            return total;
        }
        public DoubleXyz LinearAccelerationTotal()
        {
            float mass = this.Mass.Total;
            if (IfcsGenerator.getTunedMass)
            {
                mass = this.Mass.TunedMass;
            }
            
            DoubleXyz linAccTotal = new DoubleXyz(LinearThrustTotal());
            //Pos
            linAccTotal.Pos.X = linAccTotal.Pos.X/(mass);
            linAccTotal.Pos.Y = linAccTotal.Pos.Y/(mass);
            linAccTotal.Pos.Z = linAccTotal.Pos.Z/(mass);
            //Neg
            linAccTotal.Neg.X = linAccTotal.Neg.X/(mass);
            linAccTotal.Neg.Y = linAccTotal.Neg.Y/(mass);
            linAccTotal.Neg.Z = linAccTotal.Neg.Z/(mass);

            return linAccTotal;
        }

        public Mass Mass { get; set; }
        public string CruiseTime { get; set; } = "";
        public float Boost { get; set; }
        public float ABBoost { get; set; }
        public float RotationScale { get; set; }
        public DoubleXyz LinearThrust { get; set; }
        public DoubleXyz LinearScale { get; set; }
        public DoubleXyz LinearJerk { get; set; }
        public DoubleXyz LinearJerkScale { get; set; }
        public DoubleXyz AngularThrust { get; set; }
        public DoubleXyz AngularScale { get; set; }
        public SingleXyz AngularJerk { get; set; }
        public SingleXyz AngularJerkScale { get; set; }
        public float AggregateThrust { get; set; } = 0f;

        public Tuned()
        {
            this.Mass = new Mass();
            this.LinearThrust = new DoubleXyz();
            this.LinearScale = new DoubleXyz();
            this.LinearJerk = new DoubleXyz();
            this.LinearJerkScale = new DoubleXyz();
            this.AngularThrust = new DoubleXyz();
            this.AngularScale = new DoubleXyz();
            this.AngularJerk = new SingleXyz();
            this.AngularJerkScale = new SingleXyz();
        }

    }

    internal class Mass
    {
        public float Hull { get; set; }
        public float Items { get; set; }
        public float Total { get; set; }
        public float TunedMass { get; set; } = 0;
    }

    internal class DoubleXyz
    {
        public Xyz Pos { get; set; }
        public Xyz Neg { get; set; }

        internal DoubleXyz(float posX, float posY, float posZ, float negX, float negY, float negZ)
        {
            Pos = new Xyz(posX, posY, posZ);
            Neg = new Xyz(negX, negY, negZ);
        }

        internal DoubleXyz()
        {
        }

        internal DoubleXyz(DoubleXyz copy)
        {
            Pos = new Xyz(copy.Pos.X,copy.Pos.Y,copy.Pos.Z);
            Neg = new Xyz(copy.Neg.X, copy.Neg.Y, copy.Neg.Z);
        }
    }

    internal class SingleXyz
    {
        public Xyz Value { get; set; }

        internal SingleXyz(float posX, float posY, float posZ)
        {
            Value = new Xyz(posX, posY, posZ);
        }

        internal SingleXyz()
        {
            Value = new Xyz();
        }
    }

    internal class Xyz
    {
        public float X { get; set; } = 0;
        public float Y { get; set; } = 0;
        public float Z { get; set; } = 0;

        internal Xyz()
        {
        }


        internal Xyz(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        internal Xyz(string xyzString)
        {
            float[] xyzFloats = GetXYZ(xyzString);
            X = xyzFloats[0];
            Y = xyzFloats[1];
            Z = xyzFloats[2];
        }

        static float[] GetXYZ(JValue str)
        {
            return GetXYZ(str.ToString());
        }

        static float[] GetXYZ(string str)
        {
            str = str.Replace(" ", "");
            string[] strCollection = str.Split(',');
            for (int index = 0; index < strCollection.Length; index++)
            {
                string t = strCollection[index];
                if (t.StartsWith(" "))
                {
                    strCollection[index] = t.TrimStart(' ');
                }
            }

            float[] xyzFloats = new float[3];

            for (int i = 0; i < xyzFloats.Length; i++)
            {
                xyzFloats[i] = float.Parse(strCollection[i]);
            }

            return xyzFloats;
        }
    }
}