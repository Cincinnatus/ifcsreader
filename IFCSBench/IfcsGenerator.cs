﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IFCSBench
{
    class IfcsGenerator
    {

        //Re-factor for JC
        //inputs
        internal static string xmlPath = "";
        internal string modPath = xmlPath + @"\Modifications";
        internal static string Build = "";

        //Graph check box options
        internal static bool graphDistance = false;
        internal static bool graphVelocity = true;
        internal static bool graphAcceleration = true;
        internal static bool graphJerk = true;

        //Data output check box options
        internal static bool genIfcsMetaData = false;
        internal static bool genSimData = false;
        internal static bool genValidationData = false;
        internal static bool genSql = false;

        //exclusion list
        internal string[] ExclusionKeys = new string[] { "Velocity Goal", "Velocity@Goal", "Variance" };

        //historical
        internal static bool isPostABCruise = true;
        internal static Boolean IsOutputOn = false;
        internal static bool isLastMassOutputOn = false;
        internal static bool getTunedMass = false;

        internal string[] ReportKeys = new string[] {"Velocity Goal", "Velocity@Goal", "Variance"};
        internal static string REPORT_NAME = "IfcsSimulationReport";
        internal static string IFCS_EXPORT_DATA = "IfcsMetaData";
        //Get directory path

        internal static string outputPath = "";
        internal static bool BOOSTED = false;

        internal static void SetParameters(string UIXmlPath, string UiOutputPath,string UiBuild, string IfcsSchema,
            bool GuiCheckDistance, bool GuiCheckVelocity, bool GuiCheckAcceleration, bool GuiCheckJerk,
            bool GuiCheckIfcsMeta, bool GuiCheckSimulationData, bool GuiCheckValidation, bool GuiCheckSQL)
        {
            //set inputs
            xmlPath = UIXmlPath;
            outputPath = UiOutputPath;
            Build = UiBuild;
            //IfcsSchema - to be implemented later for historical functionality with older builds

            //set graph options
            graphDistance = GuiCheckDistance;
            graphVelocity = GuiCheckVelocity;
            graphAcceleration = GuiCheckAcceleration;
            graphJerk = GuiCheckJerk;

            //Set data output options
            genIfcsMetaData = GuiCheckIfcsMeta;
            genSimData = GuiCheckSimulationData;
            genValidationData = GuiCheckValidation;
            genSql = GuiCheckSQL;

        }


        internal static void Generate()
        {
            List<string> files = new List<string>();
            List<string> mods = new List<string>();

            try
            {
                //get list of files
                files = new List<string>(Directory.EnumerateFiles(xmlPath));

                //echo each file
                foreach (var file in files)
                {
                    Console.WriteLine("{0}", file.Substring(file.LastIndexOf("\\") + 1));
                }
                Console.WriteLine("{0} files found.", files.Count);
            }
            catch (UnauthorizedAccessException uaEx)
            {
                Console.WriteLine(uaEx.Message);
            }
            catch (PathTooLongException pathEx)
            {
                Console.WriteLine(pathEx.Message);
            }

            Dictionary<string, Spaceship> ships = new Dictionary<string, Spaceship>();

            GetShips(files, ships);
            RemoveInvalidShips(ships);
            

            GetVariants(ships);
            


            //Assuming mass is now correct
            //IFCSReporter.GetTunedMass(ships);
            
            //Will be re-enabled for to compare older builds
            //To-do add pre-2.6 version selection
            //IFCSReporter.GetSymmetricMotion(ships);

            IFCSReporter.GetAsymmetricMotion(ships);

            //Append Build/ID
            outputPath += @"\" + Build;
            //Create output directory if it doesn't already exist
            Directory.CreateDirectory(outputPath);

            if (genValidationData)
            {
                IFCSReporter.GetReport(ships); IFCSReporter.ExportReport(ships);
            }
            if (genIfcsMetaData) { IFCSReporter.ExportIFCSData(ships); }
            if (genSql) { IFCSReporter.ExportSQL(ships); }
            if (genSimData) { IFCSReporter.ExportMotion(ships); }



            //Test stuff
            GetGraphs(ships);

            //if (BOOSTED)
            //{
            //    foreach (var s in ships)
            //    {
            //        s.Value.GetBoosted();
            //    }
            //}




            Console.WriteLine("Done");
            Console.ReadLine();
        }

        private static void RemoveInvalidShips(Dictionary<string, Spaceship> ships)
        {
            List<string> toRemoveList = new List<string>();
            foreach (var k in ships)
            {
                if (!k.Value.isValid)
                {
                    toRemoveList.Add(k.Key);
                }
            }

            foreach (var ship in toRemoveList)
            {
                ships.Remove(ship);
            }
        }

        private static void GetGraphs(Dictionary<string, Spaceship> ships)
        {
            foreach (var k in ships)
            {
                var motions = k.Value.MotionsDictionary;
                foreach (var m in motions)
                {
                    MyGraph.GetGraph(
                        outputPath + @"\" + Build + "-" + k.Value.Localname + "-" + m.Key + ".png",
                        m.Value, Build + "-" + k.Value.Localname + "-" + m.Key);
                }
            }
        }

        private static void GetVariants(Dictionary<string, Spaceship> ships)
        {
            Dictionary<string, Spaceship> variants = new Dictionary<string, Spaceship>();
            foreach (var item in ships)
            {
                foreach (var variant in item.Value.Variants)
                {
                    variants.Add(variant.Localname, variant);
                }
            }

            foreach (var variant in variants)
            {
                ships.Add(variant.Key, variant.Value);
            }
        }

        private static void GetShips(List<string> files, Dictionary<string, Spaceship> ships)
        {
            foreach (var file in files)

            {
                var jdata = GetJson(file);

                Spaceship ship = new Spaceship(file);
                ship.LoadJson(jdata, false);

                ships.Add(ship.Localname, ship);
            }
        }

        public static dynamic GetJson(string file)
        {
            var doc = new XmlDocument();
            doc.Load(file);

            string json = JsonConvert.SerializeXmlNode(doc);
            json = json.Replace("@", "");

            dynamic jdata = JObject.Parse(json);
            return jdata;
        }

        public static dynamic GetJson(XmlDocument doc)
        {
            string json = JsonConvert.SerializeXmlNode(doc);
            json = json.Replace("@", "");

            dynamic jdata = JObject.Parse(json);
            return jdata;
        }

        public static XDocument getXDocument(string file)
        {
            string xml = File.ReadAllText(file);
            var doc = XDocument.Load(xml);
            return doc;
        }
    }
}