﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Newtonsoft.Json;

namespace IFCSBench
{
    public partial class IfcsVisualiser : Form
    {
        public IfcsVisualiser()
        {
            InitializeComponent();
            splitContainer1.SplitterDistance = 150;
            InitializeChart();

        }

        private void InitializeChart()
        {
            // create the chart
            var chart = chartIfcs;

            //create initial set
            List<MotionData> motions = new List<MotionData>();
            IFCSReporter.GetAsymmetricAxisMotion(1000,
                (float)numericAcceleration.Value,
                (float)numericJerkIn.Value,
                (float)numericJerkOut.Value,
                motions,
                (float)numericGoal.Value,
                (float)numericVelocity.Value);

            var chartArea = new ChartArea();
            chartArea.Name = "IFCS";
            //chartArea.AxisX.LabelStyle.Format = "dd/MMM\nhh:mm";
            chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisX.LabelStyle.Font = new Font("Consolas", 12);
            chartArea.AxisY.LabelStyle.Font = new Font("Consolas", 12);
            chartArea.AxisX.MinorGrid.LineColor = Color.Aquamarine;
            chartArea.AxisY.MinorGrid.LineColor = Color.Aquamarine;
            chartArea.AxisX.Title = ("Time (s)");
            chart.ChartAreas.Add(chartArea);

            List<string> legendStrings = new List<string>();

            var TimeVals = new List<string>();
            foreach (var v in motions)
            {
                TimeVals.Add(Math.Round(v.Time, 1).ToString());
            }

            if (checkDistance.Checked)
            {
                legendStrings.Add("Distance");

                var DistanceVals = new List<string>();
                foreach (var v in motions)
                {
                    DistanceVals.Add(Math.Round(v.Distance, 1).ToString());
                }

                var DistanceSeries = new Series();
                DistanceSeries.Name = "Distance";
                DistanceSeries.ChartType = SeriesChartType.FastLine;
                DistanceSeries.XValueType = ChartValueType.Auto;
                chart.Series.Add(DistanceSeries);
                //bind the datapoints
                chart.Series["Distance"].Points.DataBindXY(TimeVals, DistanceVals);

            }


            if (checkVelocity.Checked)
            {
                //Velocity
                legendStrings.Add("Velocity");

                var VelocityVals = new List<string>();
                foreach (var v in motions)
                {
                    VelocityVals.Add(Math.Round(v.Velocity, 1).ToString());
                }

                var VelocitySeries = new Series();
                VelocitySeries.Name = "Velocity";
                VelocitySeries.ChartType = SeriesChartType.FastLine;
                VelocitySeries.XValueType = ChartValueType.Auto;
                chart.Series.Add(VelocitySeries);

                // bind the datapoints
                chart.Series["Velocity"].Points.DataBindXY(TimeVals, VelocityVals);
            }


            if (checkAcceleration.Checked)
            {
                //Acceleration
                legendStrings.Add("Acceleration");
                var AccelerationVals = new List<string>();
                foreach (var v in motions)
                {
                    AccelerationVals.Add(Math.Round(v.Acceleration, 1).ToString());
                }

                var AccelerationSeries = new Series();
                AccelerationSeries.Name = "Acceleration";
                AccelerationSeries.ChartType = SeriesChartType.FastLine;
                chart.Series.Add(AccelerationSeries);

                // bind the datapoints
                chart.Series["Acceleration"].Points.DataBindXY(TimeVals, AccelerationVals);
            }


            if (checkJerk.Checked)
            {
                //Jerk
                legendStrings.Add("Jerk");
                var JerkVals = new List<string>();
                foreach (var v in motions)
                {
                    JerkVals.Add(Math.Round(v.Jerk, 1).ToString());
                }

                var JerkSeries = new Series();
                JerkSeries.Name = "Jerk";
                JerkSeries.ChartType = SeriesChartType.FastLine;
                chart.Series.Add(JerkSeries);

                // bind the datapoints
                chart.Series["Jerk"].Points.DataBindXY(TimeVals, JerkVals);
            }



            //Do the legends
            //(ms\u207b\u00b9),Acceleration (ms\u207b\u00b2),Jerk (ms\u207b\u00b3)

            string[] legendColumsn = new[] { "Color", "Label" };

            foreach (var s in legendStrings)
            {
                chart.Legends.Add(new Legend(s));
                chart.Legends[s].Docking = Docking.Left;
                chart.Legends[s].TableStyle = LegendTableStyle.Wide;
                chart.Legends[s].IsDockedInsideChartArea = true;
                chart.Legends[s].DockedToChartArea = "IFCS";
                chart.Legends[s].Alignment = StringAlignment.Near;
                chart.Legends[s].LegendStyle = LegendStyle.Row;
                chart.Legends[s].BorderWidth = 3;
                chart.Legends[s].BorderColor = Color.Beige;

                // Assign the legend to Series1.
                chart.Series[s].Legend = s;
                chart.Series[s].IsVisibleInLegend = true;

                switch (s)
                {
                    case "Distance":
                        chart.Legends[s].Title = "Distance (m)";
                        break;

                    case "Velocity":
                        chart.Legends[s].Title = "Velocity (ms\u207b\u00b3)";
                        break;

                    case "Acceleration":
                        chart.Legends[s].Title = "Acceleration (ms\u207b\u00b2)";
                        break;

                    case "Jerk":
                        chart.Legends[s].Title = "Jerk (ms\u207b\u00b3)";
                        break;

                }
            }


            var chartTitle = new Title("IFCS");
            chartTitle.Font = new Font("Arial Rounded", 20, FontStyle.Bold);
            chart.Titles.Add(chartTitle);


            // draw!
            chart.Invalidate();
            UpdateMeta(motions);
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void buttonGraph_Click(object sender, EventArgs e)
        {
            refreshIfcsChart();
        }

        private void refreshIfcsChart()
        {
            List<MotionData> motions = new List<MotionData>();
            IFCSReporter.GetAsymmetricAxisMotion(1000,
                (float) numericAcceleration.Value,
                (float) numericJerkIn.Value,
                (float) numericJerkOut.Value,
                motions,
                (float) numericGoal.Value,
                (float) numericVelocity.Value);

            var TimeVals = new List<string>();
            foreach (var v in motions)
            {
                TimeVals.Add(Math.Round(v.Time, 1).ToString());
            }

            var DistanceVals = new List<string>();
            foreach (var v in motions)
            {
                DistanceVals.Add(Math.Round(v.Distance, 1).ToString());
            }

            var VelocityVals = new List<string>();
            foreach (var v in motions)
            {
                VelocityVals.Add(Math.Round(v.Velocity, 1).ToString());
            }

            var AccelerationVals = new List<string>();
            foreach (var v in motions)
            {
                AccelerationVals.Add(Math.Round(v.Acceleration, 1).ToString());
            }

            var JerkVals = new List<string>();
            foreach (var v in motions)
            {
                JerkVals.Add(Math.Round(v.Jerk, 1).ToString());
            }

            //Clear and update existing series
            chartIfcs.Series["Distance"].Points.Clear();
            chartIfcs.Series["Distance"].Points.DataBindXY(TimeVals, DistanceVals);

            chartIfcs.Series["Velocity"].Points.Clear();
            chartIfcs.Series["Velocity"].Points.DataBindXY(TimeVals, VelocityVals);

            chartIfcs.Series["Acceleration"].Points.Clear();
            chartIfcs.Series["Acceleration"].Points.DataBindXY(TimeVals, AccelerationVals);

            chartIfcs.Series["Jerk"].Points.Clear();
            chartIfcs.Series["Jerk"].Points.DataBindXY(TimeVals, JerkVals);

            chartIfcs.Update();
            chartIfcs.Refresh();

            //update meta
            UpdateMeta(motions);
        }

        private void UpdateMeta(List<MotionData> motions)
        {
            textLastDistance.Text = motions.Last().Distance.ToString("F");
            decimal t1 = numericAcceleration.Value/numericJerkIn.Value;
            decimal t3 = numericAcceleration.Value/numericJerkOut.Value;
            decimal t2 = numericGoal.Value - t1 - t3;
            textJerkInTime.Text = t1.ToString("F");
            textAccelerationTime.Text = t2.ToString("F");
            textJerkOutTime.Text = t3.ToString("F");
            var GLimit = numericAcceleration.Value/(decimal)9.81;
            textMaxG.Text = GLimit.ToString("F");
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkDistance_CheckedChanged(object sender, EventArgs e)
        {
            chartIfcs.Series["Distance"].Enabled = checkDistance.Checked;
            chartIfcs.Update();
        }

        private void checkVelocity_CheckedChanged(object sender, EventArgs e)
        {
            chartIfcs.Series["Velocity"].Enabled = checkVelocity.Checked;
            chartIfcs.Update();
        }

        private void checkAcceleration_CheckedChanged(object sender, EventArgs e)
        {
            chartIfcs.Series["Acceleration"].Enabled = checkAcceleration.Checked;
            chartIfcs.Update();
        }

        private void checkJerk_CheckedChanged(object sender, EventArgs e)
        {
            chartIfcs.Series["Jerk"].Enabled = checkJerk.Checked;
            chartIfcs.Update();
        }
    }
}
