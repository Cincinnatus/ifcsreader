﻿namespace IFCSBench
{
    partial class IfcsInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GBox_Inputs = new System.Windows.Forms.GroupBox();
            this.label_IfcsSchema = new System.Windows.Forms.Label();
            this.Combo_IfcsSchema = new System.Windows.Forms.ComboBox();
            this.TBox_Build = new System.Windows.Forms.TextBox();
            this.Label_Build = new System.Windows.Forms.Label();
            this.LabelOutputPath = new System.Windows.Forms.Label();
            this.Button_OpenOutputPath = new System.Windows.Forms.Button();
            this.TBox_OutputPath = new System.Windows.Forms.TextBox();
            this.XMLLabel = new System.Windows.Forms.Label();
            this.Button_OpenXMLpath = new System.Windows.Forms.Button();
            this.TBox_XMLPath = new System.Windows.Forms.TextBox();
            this.GBox_GraphOptions = new System.Windows.Forms.GroupBox();
            this.check_Jerk = new System.Windows.Forms.CheckBox();
            this.check_Acceleration = new System.Windows.Forms.CheckBox();
            this.check_Velocity = new System.Windows.Forms.CheckBox();
            this.check_Distance = new System.Windows.Forms.CheckBox();
            this.GBox_Tables = new System.Windows.Forms.GroupBox();
            this.check_SQLOutput = new System.Windows.Forms.CheckBox();
            this.check_ValidationTable = new System.Windows.Forms.CheckBox();
            this.check_SimulationTable = new System.Windows.Forms.CheckBox();
            this.check_IFCSMeta = new System.Windows.Forms.CheckBox();
            this.Button_Generate = new System.Windows.Forms.Button();
            this.GBox_Generate = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonVisualiser = new System.Windows.Forms.Button();
            this.GBox_Inputs.SuspendLayout();
            this.GBox_GraphOptions.SuspendLayout();
            this.GBox_Tables.SuspendLayout();
            this.GBox_Generate.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GBox_Inputs
            // 
            this.GBox_Inputs.Controls.Add(this.label_IfcsSchema);
            this.GBox_Inputs.Controls.Add(this.Combo_IfcsSchema);
            this.GBox_Inputs.Controls.Add(this.TBox_Build);
            this.GBox_Inputs.Controls.Add(this.Label_Build);
            this.GBox_Inputs.Controls.Add(this.LabelOutputPath);
            this.GBox_Inputs.Controls.Add(this.Button_OpenOutputPath);
            this.GBox_Inputs.Controls.Add(this.TBox_OutputPath);
            this.GBox_Inputs.Controls.Add(this.XMLLabel);
            this.GBox_Inputs.Controls.Add(this.Button_OpenXMLpath);
            this.GBox_Inputs.Controls.Add(this.TBox_XMLPath);
            this.GBox_Inputs.Location = new System.Drawing.Point(12, 12);
            this.GBox_Inputs.Name = "GBox_Inputs";
            this.GBox_Inputs.Size = new System.Drawing.Size(299, 241);
            this.GBox_Inputs.TabIndex = 0;
            this.GBox_Inputs.TabStop = false;
            this.GBox_Inputs.Text = "Inputs";
            this.GBox_Inputs.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // label_IfcsSchema
            // 
            this.label_IfcsSchema.AutoSize = true;
            this.label_IfcsSchema.Location = new System.Drawing.Point(7, 173);
            this.label_IfcsSchema.Name = "label_IfcsSchema";
            this.label_IfcsSchema.Size = new System.Drawing.Size(72, 13);
            this.label_IfcsSchema.TabIndex = 9;
            this.label_IfcsSchema.Text = "IFCS Schema";
            // 
            // Combo_IfcsSchema
            // 
            this.Combo_IfcsSchema.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Combo_IfcsSchema.FormattingEnabled = true;
            this.Combo_IfcsSchema.Location = new System.Drawing.Point(10, 189);
            this.Combo_IfcsSchema.Name = "Combo_IfcsSchema";
            this.Combo_IfcsSchema.Size = new System.Drawing.Size(121, 21);
            this.Combo_IfcsSchema.TabIndex = 8;
            // 
            // TBox_Build
            // 
            this.TBox_Build.Location = new System.Drawing.Point(10, 143);
            this.TBox_Build.MaxLength = 6;
            this.TBox_Build.Name = "TBox_Build";
            this.TBox_Build.Size = new System.Drawing.Size(121, 20);
            this.TBox_Build.TabIndex = 7;
            // 
            // Label_Build
            // 
            this.Label_Build.AutoSize = true;
            this.Label_Build.Location = new System.Drawing.Point(7, 126);
            this.Label_Build.Name = "Label_Build";
            this.Label_Build.Size = new System.Drawing.Size(52, 13);
            this.Label_Build.TabIndex = 6;
            this.Label_Build.Text = "Build / ID";
            // 
            // LabelOutputPath
            // 
            this.LabelOutputPath.AutoSize = true;
            this.LabelOutputPath.Location = new System.Drawing.Point(7, 77);
            this.LabelOutputPath.Name = "LabelOutputPath";
            this.LabelOutputPath.Size = new System.Drawing.Size(71, 13);
            this.LabelOutputPath.TabIndex = 5;
            this.LabelOutputPath.Text = "Output Folder";
            // 
            // Button_OpenOutputPath
            // 
            this.Button_OpenOutputPath.Location = new System.Drawing.Point(220, 90);
            this.Button_OpenOutputPath.Name = "Button_OpenOutputPath";
            this.Button_OpenOutputPath.Size = new System.Drawing.Size(75, 23);
            this.Button_OpenOutputPath.TabIndex = 4;
            this.Button_OpenOutputPath.Text = "Browse...";
            this.Button_OpenOutputPath.UseVisualStyleBackColor = true;
            this.Button_OpenOutputPath.Click += new System.EventHandler(this.Button_OpenOutputPath_Click);
            // 
            // TBox_OutputPath
            // 
            this.TBox_OutputPath.Location = new System.Drawing.Point(10, 93);
            this.TBox_OutputPath.Name = "TBox_OutputPath";
            this.TBox_OutputPath.Size = new System.Drawing.Size(204, 20);
            this.TBox_OutputPath.TabIndex = 3;
            // 
            // XMLLabel
            // 
            this.XMLLabel.AutoSize = true;
            this.XMLLabel.Location = new System.Drawing.Point(7, 25);
            this.XMLLabel.Name = "XMLLabel";
            this.XMLLabel.Size = new System.Drawing.Size(61, 13);
            this.XMLLabel.TabIndex = 2;
            this.XMLLabel.Text = "XML Folder";
            // 
            // Button_OpenXMLpath
            // 
            this.Button_OpenXMLpath.Location = new System.Drawing.Point(220, 38);
            this.Button_OpenXMLpath.Name = "Button_OpenXMLpath";
            this.Button_OpenXMLpath.Size = new System.Drawing.Size(75, 23);
            this.Button_OpenXMLpath.TabIndex = 1;
            this.Button_OpenXMLpath.Text = "Browse...";
            this.Button_OpenXMLpath.UseVisualStyleBackColor = true;
            this.Button_OpenXMLpath.Click += new System.EventHandler(this.Button_OpenXMLpath_Click);
            // 
            // TBox_XMLPath
            // 
            this.TBox_XMLPath.Location = new System.Drawing.Point(10, 41);
            this.TBox_XMLPath.Name = "TBox_XMLPath";
            this.TBox_XMLPath.Size = new System.Drawing.Size(204, 20);
            this.TBox_XMLPath.TabIndex = 0;
            // 
            // GBox_GraphOptions
            // 
            this.GBox_GraphOptions.Controls.Add(this.check_Jerk);
            this.GBox_GraphOptions.Controls.Add(this.check_Acceleration);
            this.GBox_GraphOptions.Controls.Add(this.check_Velocity);
            this.GBox_GraphOptions.Controls.Add(this.check_Distance);
            this.GBox_GraphOptions.Location = new System.Drawing.Point(13, 259);
            this.GBox_GraphOptions.Name = "GBox_GraphOptions";
            this.GBox_GraphOptions.Size = new System.Drawing.Size(298, 73);
            this.GBox_GraphOptions.TabIndex = 1;
            this.GBox_GraphOptions.TabStop = false;
            this.GBox_GraphOptions.Text = "Graph Options";
            // 
            // check_Jerk
            // 
            this.check_Jerk.AutoSize = true;
            this.check_Jerk.Checked = true;
            this.check_Jerk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_Jerk.Location = new System.Drawing.Point(125, 42);
            this.check_Jerk.Name = "check_Jerk";
            this.check_Jerk.Size = new System.Drawing.Size(46, 17);
            this.check_Jerk.TabIndex = 3;
            this.check_Jerk.Text = "Jerk";
            this.check_Jerk.UseVisualStyleBackColor = true;
            // 
            // check_Acceleration
            // 
            this.check_Acceleration.AutoSize = true;
            this.check_Acceleration.Checked = true;
            this.check_Acceleration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_Acceleration.Location = new System.Drawing.Point(125, 19);
            this.check_Acceleration.Name = "check_Acceleration";
            this.check_Acceleration.Size = new System.Drawing.Size(85, 17);
            this.check_Acceleration.TabIndex = 2;
            this.check_Acceleration.Text = "Acceleration";
            this.check_Acceleration.UseVisualStyleBackColor = true;
            // 
            // check_Velocity
            // 
            this.check_Velocity.AutoSize = true;
            this.check_Velocity.Checked = true;
            this.check_Velocity.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_Velocity.Location = new System.Drawing.Point(6, 42);
            this.check_Velocity.Name = "check_Velocity";
            this.check_Velocity.Size = new System.Drawing.Size(63, 17);
            this.check_Velocity.TabIndex = 1;
            this.check_Velocity.Text = "Velocity";
            this.check_Velocity.UseVisualStyleBackColor = true;
            // 
            // check_Distance
            // 
            this.check_Distance.AutoSize = true;
            this.check_Distance.Location = new System.Drawing.Point(6, 19);
            this.check_Distance.Name = "check_Distance";
            this.check_Distance.Size = new System.Drawing.Size(68, 17);
            this.check_Distance.TabIndex = 0;
            this.check_Distance.Text = "Distance";
            this.check_Distance.UseVisualStyleBackColor = true;
            // 
            // GBox_Tables
            // 
            this.GBox_Tables.Controls.Add(this.check_SQLOutput);
            this.GBox_Tables.Controls.Add(this.check_ValidationTable);
            this.GBox_Tables.Controls.Add(this.check_SimulationTable);
            this.GBox_Tables.Controls.Add(this.check_IFCSMeta);
            this.GBox_Tables.Location = new System.Drawing.Point(12, 338);
            this.GBox_Tables.Name = "GBox_Tables";
            this.GBox_Tables.Size = new System.Drawing.Size(298, 73);
            this.GBox_Tables.TabIndex = 4;
            this.GBox_Tables.TabStop = false;
            this.GBox_Tables.Text = "Table Output";
            // 
            // check_SQLOutput
            // 
            this.check_SQLOutput.AutoSize = true;
            this.check_SQLOutput.Location = new System.Drawing.Point(6, 42);
            this.check_SQLOutput.Name = "check_SQLOutput";
            this.check_SQLOutput.Size = new System.Drawing.Size(47, 17);
            this.check_SQLOutput.TabIndex = 3;
            this.check_SQLOutput.Text = "SQL";
            this.check_SQLOutput.UseVisualStyleBackColor = true;
            // 
            // check_ValidationTable
            // 
            this.check_ValidationTable.AutoSize = true;
            this.check_ValidationTable.Location = new System.Drawing.Point(126, 19);
            this.check_ValidationTable.Name = "check_ValidationTable";
            this.check_ValidationTable.Size = new System.Drawing.Size(97, 17);
            this.check_ValidationTable.TabIndex = 2;
            this.check_ValidationTable.Text = "Goal Validation";
            this.check_ValidationTable.UseVisualStyleBackColor = true;
            // 
            // check_SimulationTable
            // 
            this.check_SimulationTable.AutoSize = true;
            this.check_SimulationTable.Location = new System.Drawing.Point(126, 42);
            this.check_SimulationTable.Name = "check_SimulationTable";
            this.check_SimulationTable.Size = new System.Drawing.Size(104, 17);
            this.check_SimulationTable.TabIndex = 1;
            this.check_SimulationTable.Text = "Goal Simulations";
            this.check_SimulationTable.UseVisualStyleBackColor = true;
            // 
            // check_IFCSMeta
            // 
            this.check_IFCSMeta.AutoSize = true;
            this.check_IFCSMeta.Checked = true;
            this.check_IFCSMeta.CheckState = System.Windows.Forms.CheckState.Checked;
            this.check_IFCSMeta.Location = new System.Drawing.Point(6, 19);
            this.check_IFCSMeta.Name = "check_IFCSMeta";
            this.check_IFCSMeta.Size = new System.Drawing.Size(76, 17);
            this.check_IFCSMeta.TabIndex = 0;
            this.check_IFCSMeta.Text = "IFCS Meta";
            this.check_IFCSMeta.UseVisualStyleBackColor = true;
            // 
            // Button_Generate
            // 
            this.Button_Generate.Location = new System.Drawing.Point(95, 19);
            this.Button_Generate.Name = "Button_Generate";
            this.Button_Generate.Size = new System.Drawing.Size(96, 28);
            this.Button_Generate.TabIndex = 5;
            this.Button_Generate.Text = "Generate";
            this.Button_Generate.UseVisualStyleBackColor = true;
            this.Button_Generate.Click += new System.EventHandler(this.Button_Execurte_Click);
            // 
            // GBox_Generate
            // 
            this.GBox_Generate.Controls.Add(this.Button_Generate);
            this.GBox_Generate.Location = new System.Drawing.Point(13, 418);
            this.GBox_Generate.Name = "GBox_Generate";
            this.GBox_Generate.Size = new System.Drawing.Size(294, 56);
            this.GBox_Generate.TabIndex = 7;
            this.GBox_Generate.TabStop = false;
            this.GBox_Generate.Text = "Generate!";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonVisualiser);
            this.groupBox1.Location = new System.Drawing.Point(12, 480);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(294, 56);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Visualiser";
            // 
            // buttonVisualiser
            // 
            this.buttonVisualiser.Location = new System.Drawing.Point(95, 19);
            this.buttonVisualiser.Name = "buttonVisualiser";
            this.buttonVisualiser.Size = new System.Drawing.Size(96, 28);
            this.buttonVisualiser.TabIndex = 5;
            this.buttonVisualiser.Text = "Open";
            this.buttonVisualiser.UseVisualStyleBackColor = true;
            this.buttonVisualiser.Click += new System.EventHandler(this.buttonVisualiser_Click);
            // 
            // IfcsInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 543);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GBox_Generate);
            this.Controls.Add(this.GBox_Tables);
            this.Controls.Add(this.GBox_GraphOptions);
            this.Controls.Add(this.GBox_Inputs);
            this.Name = "IfcsInputForm";
            this.Text = "IFCS Bench v0.2";
            this.Load += new System.EventHandler(this.IfcsInputForm_Load);
            this.GBox_Inputs.ResumeLayout(false);
            this.GBox_Inputs.PerformLayout();
            this.GBox_GraphOptions.ResumeLayout(false);
            this.GBox_GraphOptions.PerformLayout();
            this.GBox_Tables.ResumeLayout(false);
            this.GBox_Tables.PerformLayout();
            this.GBox_Generate.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GBox_Inputs;
        private System.Windows.Forms.Label Label_Build;
        private System.Windows.Forms.Label LabelOutputPath;
        private System.Windows.Forms.Button Button_OpenOutputPath;
        private System.Windows.Forms.TextBox TBox_OutputPath;
        private System.Windows.Forms.Label XMLLabel;
        private System.Windows.Forms.Button Button_OpenXMLpath;
        private System.Windows.Forms.TextBox TBox_XMLPath;
        private System.Windows.Forms.TextBox TBox_Build;
        private System.Windows.Forms.GroupBox GBox_GraphOptions;
        private System.Windows.Forms.CheckBox check_Jerk;
        private System.Windows.Forms.CheckBox check_Acceleration;
        private System.Windows.Forms.CheckBox check_Velocity;
        private System.Windows.Forms.CheckBox check_Distance;
        private System.Windows.Forms.GroupBox GBox_Tables;
        private System.Windows.Forms.CheckBox check_SQLOutput;
        private System.Windows.Forms.CheckBox check_ValidationTable;
        private System.Windows.Forms.CheckBox check_SimulationTable;
        private System.Windows.Forms.CheckBox check_IFCSMeta;
        private System.Windows.Forms.Button Button_Generate;
        private System.Windows.Forms.Label label_IfcsSchema;
        private System.Windows.Forms.ComboBox Combo_IfcsSchema;
        private System.Windows.Forms.GroupBox GBox_Generate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonVisualiser;
    }
}

