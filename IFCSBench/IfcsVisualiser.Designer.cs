﻿namespace IFCSBench
{
    partial class IfcsVisualiser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label4;
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonGraph = new System.Windows.Forms.Button();
            this.checkJerk = new System.Windows.Forms.CheckBox();
            this.checkAcceleration = new System.Windows.Forms.CheckBox();
            this.checkVelocity = new System.Windows.Forms.CheckBox();
            this.checkDistance = new System.Windows.Forms.CheckBox();
            this.numericGoal = new System.Windows.Forms.NumericUpDown();
            this.labelGoal = new System.Windows.Forms.Label();
            this.numericJerkOut = new System.Windows.Forms.NumericUpDown();
            this.labelJerkOut = new System.Windows.Forms.Label();
            this.numericJerkIn = new System.Windows.Forms.NumericUpDown();
            this.labelJerkIn = new System.Windows.Forms.Label();
            this.numericAcceleration = new System.Windows.Forms.NumericUpDown();
            this.labelAcceleration = new System.Windows.Forms.Label();
            this.numericVelocity = new System.Windows.Forms.NumericUpDown();
            this.labelVelocity = new System.Windows.Forms.Label();
            this.chartIfcs = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textLastDistance = new System.Windows.Forms.TextBox();
            this.labelLastV = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textJerkInTime = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textAccelerationTime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textJerkOutTime = new System.Windows.Forms.TextBox();
            this.textMaxG = new System.Windows.Forms.TextBox();
            label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericGoal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericJerkOut)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericJerkIn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAcceleration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericVelocity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartIfcs)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.chartIfcs);
            this.splitContainer1.Size = new System.Drawing.Size(954, 731);
            this.splitContainer1.SplitterDistance = 318;
            this.splitContainer1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.buttonGraph);
            this.groupBox1.Controls.Add(this.numericGoal);
            this.groupBox1.Controls.Add(this.labelGoal);
            this.groupBox1.Controls.Add(this.numericJerkOut);
            this.groupBox1.Controls.Add(this.labelJerkOut);
            this.groupBox1.Controls.Add(this.numericJerkIn);
            this.groupBox1.Controls.Add(this.labelJerkIn);
            this.groupBox1.Controls.Add(this.numericAcceleration);
            this.groupBox1.Controls.Add(this.labelAcceleration);
            this.groupBox1.Controls.Add(this.numericVelocity);
            this.groupBox1.Controls.Add(this.labelVelocity);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox1.Size = new System.Drawing.Size(318, 731);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Input";
            // 
            // buttonGraph
            // 
            this.buttonGraph.Location = new System.Drawing.Point(25, 601);
            this.buttonGraph.Name = "buttonGraph";
            this.buttonGraph.Size = new System.Drawing.Size(75, 23);
            this.buttonGraph.TabIndex = 15;
            this.buttonGraph.Text = "Update";
            this.buttonGraph.UseVisualStyleBackColor = true;
            this.buttonGraph.Click += new System.EventHandler(this.buttonGraph_Click);
            // 
            // checkJerk
            // 
            this.checkJerk.AutoSize = true;
            this.checkJerk.Checked = true;
            this.checkJerk.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkJerk.Location = new System.Drawing.Point(6, 88);
            this.checkJerk.Name = "checkJerk";
            this.checkJerk.Size = new System.Drawing.Size(46, 17);
            this.checkJerk.TabIndex = 13;
            this.checkJerk.Text = "Jerk";
            this.checkJerk.UseVisualStyleBackColor = true;
            this.checkJerk.CheckedChanged += new System.EventHandler(this.checkJerk_CheckedChanged);
            // 
            // checkAcceleration
            // 
            this.checkAcceleration.AutoSize = true;
            this.checkAcceleration.Checked = true;
            this.checkAcceleration.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkAcceleration.Location = new System.Drawing.Point(6, 65);
            this.checkAcceleration.Name = "checkAcceleration";
            this.checkAcceleration.Size = new System.Drawing.Size(85, 17);
            this.checkAcceleration.TabIndex = 12;
            this.checkAcceleration.Text = "Acceleration";
            this.checkAcceleration.UseVisualStyleBackColor = true;
            this.checkAcceleration.CheckedChanged += new System.EventHandler(this.checkAcceleration_CheckedChanged);
            // 
            // checkVelocity
            // 
            this.checkVelocity.AutoSize = true;
            this.checkVelocity.Checked = true;
            this.checkVelocity.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkVelocity.Location = new System.Drawing.Point(6, 42);
            this.checkVelocity.Name = "checkVelocity";
            this.checkVelocity.Size = new System.Drawing.Size(63, 17);
            this.checkVelocity.TabIndex = 11;
            this.checkVelocity.Text = "Velocity";
            this.checkVelocity.UseVisualStyleBackColor = true;
            this.checkVelocity.CheckedChanged += new System.EventHandler(this.checkVelocity_CheckedChanged);
            // 
            // checkDistance
            // 
            this.checkDistance.AutoSize = true;
            this.checkDistance.Checked = true;
            this.checkDistance.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkDistance.Location = new System.Drawing.Point(6, 19);
            this.checkDistance.Name = "checkDistance";
            this.checkDistance.Size = new System.Drawing.Size(68, 17);
            this.checkDistance.TabIndex = 10;
            this.checkDistance.Text = "Distance";
            this.checkDistance.UseVisualStyleBackColor = true;
            this.checkDistance.CheckedChanged += new System.EventHandler(this.checkDistance_CheckedChanged);
            // 
            // numericGoal
            // 
            this.numericGoal.DecimalPlaces = 2;
            this.numericGoal.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericGoal.Location = new System.Drawing.Point(9, 197);
            this.numericGoal.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericGoal.Name = "numericGoal";
            this.numericGoal.Size = new System.Drawing.Size(120, 20);
            this.numericGoal.TabIndex = 9;
            this.numericGoal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericGoal.Value = new decimal(new int[] {
            42,
            0,
            0,
            65536});
            this.numericGoal.ValueChanged += new System.EventHandler(this.buttonGraph_Click);
            // 
            // labelGoal
            // 
            this.labelGoal.AutoSize = true;
            this.labelGoal.Location = new System.Drawing.Point(8, 183);
            this.labelGoal.Name = "labelGoal";
            this.labelGoal.Size = new System.Drawing.Size(29, 13);
            this.labelGoal.TabIndex = 8;
            this.labelGoal.Text = "Goal";
            // 
            // numericJerkOut
            // 
            this.numericJerkOut.DecimalPlaces = 2;
            this.numericJerkOut.Location = new System.Drawing.Point(9, 158);
            this.numericJerkOut.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericJerkOut.Name = "numericJerkOut";
            this.numericJerkOut.Size = new System.Drawing.Size(120, 20);
            this.numericJerkOut.TabIndex = 7;
            this.numericJerkOut.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericJerkOut.Value = new decimal(new int[] {
            7781,
            0,
            0,
            131072});
            this.numericJerkOut.ValueChanged += new System.EventHandler(this.buttonGraph_Click);
            // 
            // labelJerkOut
            // 
            this.labelJerkOut.AutoSize = true;
            this.labelJerkOut.Location = new System.Drawing.Point(8, 144);
            this.labelJerkOut.Name = "labelJerkOut";
            this.labelJerkOut.Size = new System.Drawing.Size(47, 13);
            this.labelJerkOut.TabIndex = 6;
            this.labelJerkOut.Text = "Jerk Out";
            // 
            // numericJerkIn
            // 
            this.numericJerkIn.DecimalPlaces = 2;
            this.numericJerkIn.Location = new System.Drawing.Point(9, 119);
            this.numericJerkIn.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericJerkIn.Name = "numericJerkIn";
            this.numericJerkIn.Size = new System.Drawing.Size(120, 20);
            this.numericJerkIn.TabIndex = 5;
            this.numericJerkIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericJerkIn.Value = new decimal(new int[] {
            16208,
            0,
            0,
            131072});
            this.numericJerkIn.ValueChanged += new System.EventHandler(this.buttonGraph_Click);
            // 
            // labelJerkIn
            // 
            this.labelJerkIn.AutoSize = true;
            this.labelJerkIn.Location = new System.Drawing.Point(8, 105);
            this.labelJerkIn.Name = "labelJerkIn";
            this.labelJerkIn.Size = new System.Drawing.Size(39, 13);
            this.labelJerkIn.TabIndex = 4;
            this.labelJerkIn.Text = "Jerk In";
            // 
            // numericAcceleration
            // 
            this.numericAcceleration.DecimalPlaces = 2;
            this.numericAcceleration.Location = new System.Drawing.Point(9, 80);
            this.numericAcceleration.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericAcceleration.Name = "numericAcceleration";
            this.numericAcceleration.Size = new System.Drawing.Size(120, 20);
            this.numericAcceleration.TabIndex = 3;
            this.numericAcceleration.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericAcceleration.Value = new decimal(new int[] {
            4211,
            0,
            0,
            131072});
            this.numericAcceleration.ValueChanged += new System.EventHandler(this.buttonGraph_Click);
            // 
            // labelAcceleration
            // 
            this.labelAcceleration.AutoSize = true;
            this.labelAcceleration.Location = new System.Drawing.Point(8, 66);
            this.labelAcceleration.Name = "labelAcceleration";
            this.labelAcceleration.Size = new System.Drawing.Size(66, 13);
            this.labelAcceleration.TabIndex = 2;
            this.labelAcceleration.Text = "Acceleration";
            // 
            // numericVelocity
            // 
            this.numericVelocity.Location = new System.Drawing.Point(9, 41);
            this.numericVelocity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericVelocity.Name = "numericVelocity";
            this.numericVelocity.Size = new System.Drawing.Size(120, 20);
            this.numericVelocity.TabIndex = 1;
            this.numericVelocity.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericVelocity.Value = new decimal(new int[] {
            160,
            0,
            0,
            0});
            this.numericVelocity.ValueChanged += new System.EventHandler(this.buttonGraph_Click);
            // 
            // labelVelocity
            // 
            this.labelVelocity.AutoSize = true;
            this.labelVelocity.Location = new System.Drawing.Point(8, 27);
            this.labelVelocity.Name = "labelVelocity";
            this.labelVelocity.Size = new System.Drawing.Size(44, 13);
            this.labelVelocity.TabIndex = 0;
            this.labelVelocity.Text = "Velocity";
            // 
            // chartIfcs
            // 
            this.chartIfcs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartIfcs.Location = new System.Drawing.Point(0, 0);
            this.chartIfcs.Name = "chartIfcs";
            this.chartIfcs.Size = new System.Drawing.Size(632, 731);
            this.chartIfcs.TabIndex = 0;
            this.chartIfcs.Text = "chart1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkDistance);
            this.groupBox2.Controls.Add(this.checkVelocity);
            this.groupBox2.Controls.Add(this.checkAcceleration);
            this.groupBox2.Controls.Add(this.checkJerk);
            this.groupBox2.Location = new System.Drawing.Point(9, 472);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(121, 123);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Series Selection";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(label4);
            this.groupBox3.Controls.Add(this.textMaxG);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.textJerkOutTime);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.textAccelerationTime);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.textJerkInTime);
            this.groupBox3.Controls.Add(this.labelLastV);
            this.groupBox3.Controls.Add(this.textLastDistance);
            this.groupBox3.Location = new System.Drawing.Point(10, 237);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(120, 229);
            this.groupBox3.TabIndex = 17;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Meta";
            // 
            // textLastDistance
            // 
            this.textLastDistance.Location = new System.Drawing.Point(5, 36);
            this.textLastDistance.Name = "textLastDistance";
            this.textLastDistance.ReadOnly = true;
            this.textLastDistance.Size = new System.Drawing.Size(100, 20);
            this.textLastDistance.TabIndex = 0;
            // 
            // labelLastV
            // 
            this.labelLastV.AutoSize = true;
            this.labelLastV.Location = new System.Drawing.Point(2, 20);
            this.labelLastV.Name = "labelLastV";
            this.labelLastV.Size = new System.Drawing.Size(49, 13);
            this.labelLastV.TabIndex = 1;
            this.labelLastV.Text = "Distance";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Jerk In Time";
            // 
            // textJerkInTime
            // 
            this.textJerkInTime.Location = new System.Drawing.Point(5, 75);
            this.textJerkInTime.Name = "textJerkInTime";
            this.textJerkInTime.ReadOnly = true;
            this.textJerkInTime.Size = new System.Drawing.Size(100, 20);
            this.textJerkInTime.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Acceleration Time";
            // 
            // textAccelerationTime
            // 
            this.textAccelerationTime.Location = new System.Drawing.Point(5, 114);
            this.textAccelerationTime.Name = "textAccelerationTime";
            this.textAccelerationTime.ReadOnly = true;
            this.textAccelerationTime.Size = new System.Drawing.Size(100, 20);
            this.textAccelerationTime.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(2, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Jerk Out Time";
            // 
            // textJerkOutTime
            // 
            this.textJerkOutTime.Location = new System.Drawing.Point(5, 153);
            this.textJerkOutTime.Name = "textJerkOutTime";
            this.textJerkOutTime.ReadOnly = true;
            this.textJerkOutTime.Size = new System.Drawing.Size(100, 20);
            this.textJerkOutTime.TabIndex = 6;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(3, 176);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(65, 13);
            label4.TabIndex = 9;
            label4.Text = "Max G-Load";
            // 
            // textMaxG
            // 
            this.textMaxG.Location = new System.Drawing.Point(5, 192);
            this.textMaxG.Name = "textMaxG";
            this.textMaxG.ReadOnly = true;
            this.textMaxG.Size = new System.Drawing.Size(100, 20);
            this.textMaxG.TabIndex = 8;
            this.textMaxG.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // IfcsVisualiser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(954, 731);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "IfcsVisualiser";
            this.Text = "IFCS Visualiser";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericGoal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericJerkOut)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericJerkIn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAcceleration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericVelocity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartIfcs)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelVelocity;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartIfcs;
        private System.Windows.Forms.NumericUpDown numericGoal;
        private System.Windows.Forms.Label labelGoal;
        private System.Windows.Forms.NumericUpDown numericJerkOut;
        private System.Windows.Forms.Label labelJerkOut;
        private System.Windows.Forms.NumericUpDown numericJerkIn;
        private System.Windows.Forms.Label labelJerkIn;
        private System.Windows.Forms.NumericUpDown numericAcceleration;
        private System.Windows.Forms.Label labelAcceleration;
        private System.Windows.Forms.NumericUpDown numericVelocity;
        private System.Windows.Forms.CheckBox checkJerk;
        private System.Windows.Forms.CheckBox checkAcceleration;
        private System.Windows.Forms.CheckBox checkVelocity;
        private System.Windows.Forms.CheckBox checkDistance;
        private System.Windows.Forms.Button buttonGraph;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelLastV;
        private System.Windows.Forms.TextBox textLastDistance;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textJerkOutTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textAccelerationTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textJerkInTime;
        private System.Windows.Forms.TextBox textMaxG;
    }
}