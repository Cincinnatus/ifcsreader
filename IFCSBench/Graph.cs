﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms.DataVisualization.Charting;

namespace IFCSBench
{
    public class MyGraph
    {
        public static void GetGraph(string path, List<MotionData> motions, string ship)
        {

            // create the chart
            var chart = new Chart();
            chart.Size = new Size(1920, 1080);

            var chartArea = new ChartArea();
            chartArea.Name = ship;
            //chartArea.AxisX.LabelStyle.Format = "dd/MMM\nhh:mm";
            chartArea.AxisX.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisY.MajorGrid.LineColor = Color.LightGray;
            chartArea.AxisX.LabelStyle.Font = new Font("Consolas", 12);
            chartArea.AxisY.LabelStyle.Font = new Font("Consolas", 12);
            chartArea.AxisX.MinorGrid.LineColor = Color.Aquamarine;
            chartArea.AxisY.MinorGrid.LineColor = Color.Aquamarine;
            chartArea.AxisX.Title = ("Time (s)");
            chart.ChartAreas.Add(chartArea);

            List<string> legendStrings = new List<string>();

            var TimeVals = new List<string>();
            foreach (var v in motions)
            {
                TimeVals.Add(Math.Round(v.Time, 1).ToString());
            }

            if (IfcsGenerator.graphDistance)
            {
                legendStrings.Add("Distance");

                var DistanceVals = new List<string>();
                foreach (var v in motions)
                {
                    DistanceVals.Add(Math.Round(v.Distance, 1).ToString());
                }

                var DistanceSeries = new Series();
                DistanceSeries.Name = "Distance";
                DistanceSeries.ChartType = SeriesChartType.FastLine;
                DistanceSeries.XValueType = ChartValueType.Auto;
                chart.Series.Add(DistanceSeries);
                //bind the datapoints
                chart.Series["Distance"].Points.DataBindXY(TimeVals, DistanceVals);

            }


            if (IfcsGenerator.graphVelocity)
            {
                //Velocity
                legendStrings.Add("Velocity");

                var VelocityVals = new List<string>();
                foreach (var v in motions)
                {
                    VelocityVals.Add(Math.Round(v.Velocity, 1).ToString());
                }

                var VelocitySeries = new Series();
                VelocitySeries.Name = "Velocity";
                VelocitySeries.ChartType = SeriesChartType.FastLine;
                VelocitySeries.XValueType = ChartValueType.Auto;
                chart.Series.Add(VelocitySeries);

                // bind the datapoints
                chart.Series["Velocity"].Points.DataBindXY(TimeVals, VelocityVals);
            }


            if (IfcsGenerator.graphAcceleration)
            {
                //Acceleration
                legendStrings.Add("Acceleration");
                var AccelerationVals = new List<string>();
                foreach (var v in motions)
                {
                    AccelerationVals.Add(Math.Round(v.Acceleration, 1).ToString());
                }

                var AccelerationSeries = new Series();
                AccelerationSeries.Name = "Acceleration";
                AccelerationSeries.ChartType = SeriesChartType.FastLine;
                chart.Series.Add(AccelerationSeries);

                // bind the datapoints
                chart.Series["Acceleration"].Points.DataBindXY(TimeVals, AccelerationVals);
            }


            if (IfcsGenerator.graphJerk)
            {
                //Jerk
                legendStrings.Add("Jerk");
                var JerkVals = new List<string>();
                foreach (var v in motions)
                {
                    JerkVals.Add(Math.Round(v.Jerk, 1).ToString());
                }

                var JerkSeries = new Series();
                JerkSeries.Name = "Jerk";
                JerkSeries.ChartType = SeriesChartType.FastLine;
                chart.Series.Add(JerkSeries);

                // bind the datapoints
                chart.Series["Jerk"].Points.DataBindXY(TimeVals, JerkVals);
            }



            //Do the legends
            //(ms\u207b\u00b9),Acceleration (ms\u207b\u00b2),Jerk (ms\u207b\u00b3)

            string[] legendColumsn = new[] {"Color", "Label"};

            foreach (var s in legendStrings)
            {
                chart.Legends.Add(new Legend(s));
                chart.Legends[s].Docking = Docking.Left;
                chart.Legends[s].TableStyle = LegendTableStyle.Wide;
                chart.Legends[s].IsDockedInsideChartArea = true;
                chart.Legends[s].DockedToChartArea = ship;
                chart.Legends[s].Alignment = StringAlignment.Near;
                chart.Legends[s].LegendStyle = LegendStyle.Row;
                chart.Legends[s].BorderWidth = 3;
                chart.Legends[s].BorderColor = Color.Beige;

                // Assign the legend to Series1.
                chart.Series[s].Legend = s;
                chart.Series[s].IsVisibleInLegend = true;

                switch (s)
                {
                    case "Distance":
                        chart.Legends[s].Title = "Distance (m)";
                        break;

                    case "Velocity":
                        chart.Legends[s].Title = "Velocity (ms\u207b\u00b3)";
                        break;

                    case "Acceleration":
                        chart.Legends[s].Title = "Acceleration (ms\u207b\u00b2)";
                        break;

                    case "Jerk":
                        chart.Legends[s].Title = "Jerk (ms\u207b\u00b3)";
                        break;

                }
            }


            var chartTitle = new Title(ship);
            chartTitle.Font = new Font("Arial Rounded",20,FontStyle.Bold);
            chart.Titles.Add(chartTitle);
            

            // draw!
            chart.Invalidate();

            // write out a file
            chart.SaveImage(path, ChartImageFormat.Png);
        }
    }
}