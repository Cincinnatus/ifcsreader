﻿namespace IFCSBench
{
    public struct MotionData
    {
        public double Time, Distance, Velocity, Acceleration, Jerk;

        public MotionData(double time, double distance, double velocity, double acceleration, double jerk)
        {
            this.Time = time;
            this.Distance = distance;
            this.Velocity = velocity;
            this.Acceleration = acceleration;
            this.Jerk = jerk;
        }
    }
}
