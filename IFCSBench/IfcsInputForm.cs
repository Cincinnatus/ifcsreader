﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IFCSBench
{
    public partial class IfcsInputForm : Form
    {
        public IfcsInputForm()
        {
            InitializeComponent();
            InitializeComboBox();
        }

        private void InitializeComboBox()
        {
            Combo_IfcsSchema.Items.Add(new ComboBoxItem("485232", "485232"));
            Combo_IfcsSchema.SelectedIndex = 0;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Button_OpenXMLpath_Click(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                TBox_XMLPath.Text = dialog.SelectedPath;
            }
            
        }

        private void Button_OpenOutputPath_Click(object sender, EventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {

                TBox_OutputPath.Text = dialog.SelectedPath;
            }
        }

        private void Button_Execurte_Click(object sender, EventArgs e)
        {

            if (!isIfcsInputFormValid())
            {
                return;
            }

            Button_Generate.Text = "Working...";
            Button_Generate.Enabled = false;
            
            IfcsGenerator.SetParameters(TBox_XMLPath.Text,TBox_OutputPath.Text,TBox_Build.Text,Combo_IfcsSchema.SelectedText,
                check_Distance.Checked,check_Velocity.Checked,check_Acceleration.Checked,check_Jerk.Checked,
                check_IFCSMeta.Checked,check_SimulationTable.Checked,check_ValidationTable.Checked,check_SQLOutput.Checked);
            IfcsGenerator.Generate();

            Button_Generate.Enabled = true;
            Button_Generate.Text = "Generate";
        }

        private bool isIfcsInputFormValid()
        {



            if (TBox_XMLPath.Text.Length == 0)
            {
                MessageBox.Show("No XML Path Provided");
                return false;
                
            }

            if (TBox_OutputPath.Text.Length == 0)
            {
                MessageBox.Show("No Output Path Provided");
                return false;
            }

            if (TBox_Build.Text.Length == 0)
            {
                MessageBox.Show("No Build ID Provided");
                return false;
            }

            //file and folder generation

            if (!Directory.Exists(TBox_XMLPath.Text))
            {
                MessageBox.Show("XML Path does not exist");
                return false;
            }

            if (!Directory.Exists(TBox_OutputPath.Text))
            {
                MessageBox.Show("Output Path does not exist");
                return false;
            }

            if (!isValidFileName(TBox_Build.Text))
            {
                MessageBox.Show(@"Invalid build ID, cannot contain the following: < > : \ / | ? *");
                return false;
            }


            return true;
        }

        private bool isValidFileName(string fileName)
        {
            if (fileName.Contains("<") ||
                fileName.Contains(">") ||
                fileName.Contains(":") ||
                fileName.Contains("\"") ||
                fileName.Contains("\\") ||
                fileName.Contains("/") ||
                fileName.Contains("|") ||
                fileName.Contains("?") ||
                fileName.Contains("*"))
            {
                return false;
            }
            return true;
        }

        private void IfcsInputForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonVisualiser_Click(object sender, EventArgs e)
        {
            IfcsVisualiser vis = new IfcsVisualiser();
            vis.Show();
        }
    }
}
