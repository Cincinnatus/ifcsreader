﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace IFCSBench
{
    internal class IFCSReporter
    {
        internal static void GetSymmetricMotion(Dictionary<string, Spaceship> ships)
        {
            foreach (var item in ships)
            {
                int resolution = 1000;
                Spaceship spaceship = item.Value;

                //template
                //GetSymmetricAxisMotion(resolution, acceleration, jerkIn, motion, TimeGoal, SCMVelocity);
                //do x-pos
                var timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Neg.X;
                List<MotionData> posXMotion = new List<MotionData>();
                GetSymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Pos.X,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.X,
                    posXMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("pos-x", posXMotion);
                //--------------------------------------
                //do y-pos
                //
                //--Test--
                //var testLinearJerk = spaceship.Ifcs.Tuned.LinearJerk.Pos.Y;
                //var testLinearJerkScale = spaceship.Ifcs.Tuned.LinearJerkScale.Pos.Y;
                //var testLinearJerkTotal = spaceship.Ifcs.Tuned.LinearJerkTotal().Pos.Y;

                //
                //
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Neg.Y;
                List<MotionData> posYMotion = new List<MotionData>();
                GetSymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Y,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.Y,
                    posYMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("pos-y", posYMotion);
                //--------------------------------------


                //do z-pos
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Neg.Z;
                List<MotionData> posZMotion = new List<MotionData>();
                GetSymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Z,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.Z,
                    posZMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("pos-z", posZMotion);
                //--------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                //do x-neg
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Pos.X;
                List<MotionData> negXMotion = new List<MotionData>();
                GetSymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Neg.X,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.X,
                    negXMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("neg-x", negXMotion);

                //do y-neg
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Pos.Y;
                List<MotionData> negYMotion = new List<MotionData>();
                GetSymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Y,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.Y,
                    negYMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("neg-y", negYMotion);


                //do z-neg
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Pos.Z;
                List<MotionData> negZMotion = new List<MotionData>();
                GetSymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Z,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.Z,
                    negZMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("neg-z", negZMotion);
            }
        }

        internal static void GetSymmetricAxisMotion(int resolution, float acceleration, float jerk,
            List<MotionData> motion,
            float timeGoal,
            float scmVelocity)
        {
            double interval = 1f/resolution;

            double jerkTime = acceleration/jerk;
            Boolean isP1Complete = false;
            double time = interval;
            int samples = 1 + (int) (timeGoal*resolution);

            //T-zero
            motion.Add(new MotionData(0, 0, 0, 0, 0));


            //sample.IFCS.Tuning.SCMVelocity
            //while velocity less than max
            for (int i = 1; i < samples; i++)
            {
                //Phase 1: Acceleration build up
                if (motion[motion.Count - 1].Acceleration < acceleration && !isP1Complete)
                {
                    double a = motion.Last().Acceleration + jerk*interval;
                    double v = motion.Last().Velocity + a*interval + 0.5f*jerk*interval*interval;
                    double d = motion.Last().Distance
                               + v*interval
                               + 0.5f*a*interval*interval
                               + (1/6f)*jerk*interval*interval*interval;

                    if (a >= acceleration)
                    {
                        isP1Complete = true;
                    }

                    motion.Add(new MotionData(time, d, v, a, jerk));
                }
                //phase 2: Limit acceleration
                else if (time < timeGoal - jerkTime)
                {
                    double a = acceleration;
                    double v = motion.Last().Velocity + a*interval;
                    double d = motion.Last().Distance
                               + v*interval
                               + 0.5f*a*interval*interval
                               + (1/6f)*jerk*interval*interval*interval;
                    motion.Add(new MotionData(time, d, v, a, 0));
                }
                else
                {
                    double a = motion.Last().Acceleration - jerk*interval;
                    double v = motion.Last().Velocity + a*interval + 0.5f*jerk*interval*interval;
                    double d = motion.Last().Distance
                               + v*interval
                               + 0.5f*a*interval*interval
                               + (1/6f)*jerk*interval*interval*interval;

                    motion.Add(new MotionData(time, d, v, a, -jerk));
                }

                if (IfcsGenerator.IsOutputOn)
                {
                    Console.WriteLine("------------------------------------");
                    Console.WriteLine("Time: " + time);
                    Console.WriteLine("Jerk: " + motion.Last().Jerk);
                    Console.WriteLine("Acceleration: " + motion.Last().Acceleration);
                    Console.WriteLine("Velocity: " + motion.Last().Velocity);
                    Console.WriteLine("Distance: " + motion.Last().Distance);
                    Console.WriteLine("MaxV: " + scmVelocity);
                }


                if ((time >= timeGoal)
                    || (motion.Last().Velocity >= scmVelocity))
                {
                    //has reached goal
                    break;
                }

                Math.Round(time += interval, 2);
            }
        }


        internal static void GetAsymmetricMotion(Dictionary<string, Spaceship> ships)
        {
            foreach (var item in ships)
            {
                int resolution = 1000;
                Spaceship spaceship = item.Value;

                //template
                //GetAsymmetricAxisMotion(resolution, acceleration, jerkIn, motion, TimeGoal, SCMVelocity);
                //do x-pos
                var timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Neg.X;
                List<MotionData> posXMotion = new List<MotionData>();
                GetAsymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Pos.X,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.X,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.X,
                    posXMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("pos-x", posXMotion);
                //--------------------------------------
                //do y-pos
                //
                //--Test--
                //var testLinearJerk = spaceship.Ifcs.Tuned.LinearJerk.Pos.Y;
                //var testLinearJerkScale = spaceship.Ifcs.Tuned.LinearJerkScale.Pos.Y;
                //var testLinearJerkTotal = spaceship.Ifcs.Tuned.LinearJerkTotal().Pos.Y;

                //
                //
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Neg.Y;
                List<MotionData> posYMotion = new List<MotionData>();
                GetAsymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Y,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.Y,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.Y,
                    posYMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("pos-y", posYMotion);
                //--------------------------------------


                //do z-pos
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Neg.Z;
                List<MotionData> posZMotion = new List<MotionData>();
                GetAsymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Z,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.Z,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.Z,
                    posZMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("pos-z", posZMotion);
                //--------->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                //do x-neg
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Pos.X;
                List<MotionData> negXMotion = new List<MotionData>();
                GetAsymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Neg.X,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.X,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.X,
                    negXMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("neg-x", negXMotion);

                //do y-neg
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Pos.Y;
                List<MotionData> negYMotion = new List<MotionData>();
                GetAsymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Y,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.Y,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.Y,
                    negYMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("neg-y", negYMotion);


                //do z-neg
                timeGoal = spaceship.Ifcs.GoalStopTimes.Linear.Pos.Z;
                List<MotionData> negZMotion = new List<MotionData>();
                GetAsymmetricAxisMotion(resolution,
                    spaceship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Z,
                    spaceship.Ifcs.Tuned.LinearJerk.Neg.Z,
                    spaceship.Ifcs.Tuned.LinearJerk.Pos.Z,
                    negZMotion,
                    timeGoal,
                    spaceship.Ifcs.Tuning.ScmVelocity);
                //Add to Dictionary
                item.Value.MotionsDictionary.Add("neg-z", negZMotion);
            }
        }

        internal static void GetAsymmetricAxisMotion(int resolution, float acceleration, float jerkIn, float jerkOut,
            List<MotionData> motion,
            float timeGoal,
            float scmVelocity)
        {
            double interval = 1f / resolution;

            double jerkOutTime = acceleration / jerkOut;
            Boolean isP1Complete = false;
            double time = interval;
            int samples = 1 + (int)(timeGoal * resolution);

            //T-zero
            motion.Add(new MotionData(0, 0, 0, 0, 0));


            //sample.IFCS.Tuning.SCMVelocity
            //while velocity less than max
            for (int i = 1; i < samples; i++)
            {
                //Phase 1: Acceleration build up
                if (motion[motion.Count - 1].Acceleration < acceleration && !isP1Complete)
                {
                    double a = motion.Last().Acceleration + jerkIn * interval;
                    double v = motion.Last().Velocity + a * interval + 0.5f * jerkIn * interval * interval;
                    double d = motion.Last().Distance
                               + v * interval
                               + 0.5f * a * interval * interval
                               + (1 / 6f) * jerkIn * interval * interval * interval;

                    if (a >= acceleration)
                    {
                        isP1Complete = true;
                    }

                    motion.Add(new MotionData(time, d, v, a, jerkIn));
                }
                //phase 2: Limit acceleration
                else if (time < timeGoal - jerkOutTime)
                {
                    double a = acceleration;
                    double v = motion.Last().Velocity + a * interval;
                    double d = motion.Last().Distance
                               + v * interval
                               + 0.5f * a * interval * interval
                               + (1 / 6f) * jerkIn * interval * interval * interval;
                    motion.Add(new MotionData(time, d, v, a, 0));
                }
                else
                {
                    double a = motion.Last().Acceleration - jerkOut * interval;
                    double v = motion.Last().Velocity + a * interval + 0.5f * jerkOut * interval * interval;
                    double d = motion.Last().Distance
                               + v * interval
                               + 0.5f * a * interval * interval
                               + (1 / 6f) * jerkOut * interval * interval * interval;

                    motion.Add(new MotionData(time, d, v, a, -jerkOut));
                }

                if (IfcsGenerator.IsOutputOn)
                {
                    Console.WriteLine("------------------------------------");
                    Console.WriteLine("Time: " + time);
                    Console.WriteLine("Jerk: " + motion.Last().Jerk);
                    Console.WriteLine("Acceleration: " + motion.Last().Acceleration);
                    Console.WriteLine("Velocity: " + motion.Last().Velocity);
                    Console.WriteLine("Distance: " + motion.Last().Distance);
                    Console.WriteLine("MaxV: " + scmVelocity);
                }


                if ((time >= timeGoal)
                    || (motion.Last().Velocity >= scmVelocity))
                {
                    //has reached goal
                    break;
                }

                Math.Round(time += interval, 2);
            }
        }


        internal static void ExportReport(Dictionary<string, Spaceship> ships)
        {
            List<String> fileOut = new List<string>();
            //"Velocity Goal", "Velocity@Goal", "Variance"
            fileOut.Add("Ship,Axis,Velocity Goal,Velocity@Goal,Variance,");

            foreach (var item in ships)
            {
                var finalVelocity = item.Value.Ifcs.Tuning.ScmVelocity;
                foreach (var axis in item.Value.GetReports())
                {
                    string s = item.Value.Localname + ',' + axis.AxisLabel + ',' + finalVelocity + ',' +
                               axis.VelocityAtGoal + ',' + axis.Variance;
                    fileOut.Add(s);
                }
            }

            StreamWriter fifoStreamWriter =
                new StreamWriter(
                    IfcsGenerator.outputPath + @"\" + IfcsGenerator.Build + "-" + IfcsGenerator.REPORT_NAME +
                    ".csv", false,
                    Encoding.UTF8);

            foreach (string line in fileOut)
            {
                fifoStreamWriter.WriteLine(line);
            }

            fifoStreamWriter.Flush();
            fifoStreamWriter.Close();
        }

        internal static void GetReport(Dictionary<string, Spaceship> ships)
        {
            foreach (KeyValuePair<string, Spaceship> s in ships)
            {
                //Get intended velocity
                var intendedVelocity = s.Value.Ifcs.Tuning.ScmVelocity;

                foreach (var motionSet in s.Value.MotionsDictionary)
                {
                    //"Velocity Goal", "Velocity@Goal", "Variance"};
                    //get Axis
                    var motionKey = motionSet.Key;
                    //get finalVelocity
                    var finalVelocity = motionSet.Value.Last().Velocity;
                    //Add profile data "Velocity Goal", "Velocity@Goal", "Variance"};
                    s.Value.AddReport(new Report(motionKey, Math.Round(finalVelocity),
                        Math.Round(intendedVelocity - finalVelocity)));
                }
            }
        }

        internal static void ExportMotion(Dictionary<string, Spaceship> shipMotions)
        {
            foreach (var item in shipMotions)
            {
                foreach (var axis in item.Value.MotionsDictionary)
                {
                    var motion = axis.Value;
                    int i;
                    List<String> fileOut = new List<string>();
                    fileOut.Add(
                        "Time,Distance,Velocity (ms\u207b\u00b9),Acceleration (ms\u207b\u00b2),Jerk (ms\u207b\u00b3)");
                    foreach (var row in motion)
                    {
                        string s = row.Time.ToString() + ',' + Math.Round(row.Distance, 3) + ',' +
                                   Math.Round(row.Velocity, 3) + ',' +
                                   Math.Round(row.Acceleration, 3) + ',' + Math.Round(row.Jerk, 3);
                        fileOut.Add(s);
                    }

                    StreamWriter fifoStreamWriter =
                        new StreamWriter(
                            IfcsGenerator.outputPath + @"\" + IfcsGenerator.Build + "-" +
                            item.Key + "-" +
                            axis.Key + ".csv", false,
                            Encoding.UTF8);

                    foreach (string line in fileOut)
                    {
                        fifoStreamWriter.WriteLine(line);
                    }

                    fifoStreamWriter.Flush();
                    fifoStreamWriter.Close();
                }
            }
        }


        internal static void ExportIFCSData(Dictionary<string, Spaceship> ships)
        {
            List<String> fileOut = new List<string>();
            //"Velocity Goal", "Velocity@Goal", "Variance"
            fileOut.Add("LocalName,Shipname,Shipbase," +
                        //mass
                        "mass-hull,mass-items,mass-total,mass-tuned," +
                        //vCaps
                        "SCM,ABM,CM," +
                        //Goal times
                        "Linear-Goal-Pos-X,Linear-Goal-Pos-Y,Linear-Goal-Pos-Z," +
                        "Linear-Goal-Neg-X,Linear-Goal-Neg-Y,Linear-Goal-Neg-Z," +
                        //Linear y-neg comparison
                        "Mains-Ratio-Pos-X,Mains-Ratio-Pos-Y,Mains-Ratio-Pos-Z," +
                        "Mains-Ratio-Neg-X,Mains-Ratio-Neg-Y,Mains-Ratio-Neg-Z," +
                        //Aggregate thrust
                        "Aggregate thrust," +
                        //Linear thrust acceleration
                        "Linear-Acceleration-Pos-X,Linear-Acceleration-Pos-Y,Linear-Acceleration-Pos-Z," +
                        "Linear-Acceleration-Neg-X,Linear-Acceleration-Neg-Y,Linear-Acceleration-Neg-Z," +
                        //Linear thrust values
                        "Linear-Thrust-Pos-X,Linear-Thrust-Pos-Y,Linear-Thrust-Pos-Z," +
                        "Linear-Thrust-Neg-X,Linear-Thrust-Neg-Y,Linear-Thrust-Neg-Z," +
                        //Linear jerkIn
                        "Linear-jerkIn-Pos-X,Linear-jerkIn-Pos-Y,Linear-jerkIn-Pos-Z," +
                        "Linear-jerkIn-Neg-X,Linear-jerkIn-Neg-Y,Linear-jerkIn-Neg-Z," +
                        //Angular Goal times
                        "Angular-Goal-X,Angular-Goal-Y,Angular-Goal-Z," +
                        //Angular velocity
                        "Angular-V-X,Angular-V-Y,Angular-V-Z," +
                        //Angular thrust values
                        "Angular-Thrust-Pos-X,Angular-Thrust-Pos-Y,Angular-Thrust-Pos-Z," +
                        "Angular-Thrust-Neg-X,Angular-Thrust-Neg-Y,Angular-Thrust-Neg-Z," +
                        //Angular Jerk
                        "Angular-jerkIn-X,Angular-jerkIn-Y,Angular-jerkIn-Z," +
                        //Other
                        "Boost," +
                        "AB-Boost," +
                        //Boosted Values
                        //Goal times
                        "SCM-Goal-Pos-X,SCM-Goal-Pos-Y,SCM-Goal-Pos-Z," +
                        "SCM-Goal-Neg-X,SCM-Goal-Neg-Y,SCM-Goal-Neg-Z," +

                        //Goal times
                        "ABM-Goal-Pos-X,ABM-Goal-Pos-Y,ABM-Goal-Pos-Z," +
                        "ABM-Goal-Neg-X,ABM-Goal-Neg-Y,ABM-Goal-Neg-Z," +

                        //Goal distance
                        "ABM-Dist-Pos-X,ABM-Dist-Pos-Y,ABM-Dist-Pos-Z," +
                        "ABM-Dist-Neg-X,ABM-Dist-Neg-Y,ABM-Dist-Neg-Z," +

                        //Aggregate thrust
                        "Boosted Total Thrust," +
                        //Linear thrust acceleration
                        "Boosted-Acceleration-Pos-X,Boosted-Acceleration-Pos-Y,Boosted-Acceleration-Pos-Z," +
                        "Boosted-Acceleration-Neg-X,Boosted-Acceleration-Neg-Y,Boosted-Acceleration-Neg-Z," +
                        //Boosted thrust values
                        "Boosted-Thrust-Pos-X,Boosted-Thrust-Pos-Y,Boosted-Thrust-Pos-Z," +
                        "Boosted-Thrust-Neg-X,Boosted-Thrust-Neg-Y,Boosted-Thrust-Neg-Z," +
                        //Boosted jerkIn
                        "Boosted-jerkIn-Pos-X,Boosted-jerkIn-Pos-Y,Boosted-jerkIn-Pos-Z," +
                        "Boosted-jerkIn-Neg-X,Boosted-jerkIn-Neg-Y,Boosted-jerkIn-Neg-Z," +
                        "Rotation Scale"
                );

            foreach (var item in ships)
            {
                var ship = item.Value;

                {
                    string s =
                        //Name/modification meta
                        ship.Localname + ',' +
                        ship.Shipname + ',' +
                        ship.Shipbase + ',' +
                        //Ship mass
                        ship.Ifcs.Tuned.Mass.Hull + ',' +
                        ship.Ifcs.Tuned.Mass.Items + ',' +
                        ship.Ifcs.Tuned.Mass.Total + ',' +
                        ship.Ifcs.Tuned.Mass.TunedMass + ',' +
                        //Tuning speeds
                        ship.Ifcs.Tuning.ScmVelocity + ',' +
                        ship.Ifcs.Tuning.AbmVelocity + ',' +
                        ship.Ifcs.Tuning.CmVelocity + ',' +
                        //Goal stop times
                        ship.Ifcs.GoalStopTimes.Linear.Pos.X + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Pos.Y + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Pos.Z + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Neg.X + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Neg.Y + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Neg.Z + ',' +

                        //mains ratio time
                        ship.Ifcs.GoalStopTimes.GetMainsRatio().Pos.X + ',' +
                        ship.Ifcs.GoalStopTimes.GetMainsRatio().Pos.Y + ',' +
                        ship.Ifcs.GoalStopTimes.GetMainsRatio().Pos.Z + ',' +
                        ship.Ifcs.GoalStopTimes.GetMainsRatio().Neg.X + ',' +
                        ship.Ifcs.GoalStopTimes.GetMainsRatio().Neg.Y + ',' +
                        ship.Ifcs.GoalStopTimes.GetMainsRatio().Neg.Z + ',' +

                        //aggregate thrust
                        ship.Ifcs.Tuned.AggregateThrust.ToString("F") + ',' +

                        //Linear accelerations totals
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Z.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Z.ToString("F") + ',' +
                        //Linear thrust totals
                        ship.Ifcs.Tuned.LinearThrustTotal().Pos.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Pos.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Pos.Z.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Neg.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Neg.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Neg.Z.ToString("F") + ',' +
                        //Linear jerkIn values
                        ship.Ifcs.Tuned.LinearJerk.Pos.X + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Pos.Y + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Pos.Z + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Neg.X + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Neg.Y + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Neg.Z + ',' +

                        //Angular Goals
                        ship.Ifcs.GoalStopTimes.Angular.Value.X + ',' +
                        ship.Ifcs.GoalStopTimes.Angular.Value.Y + ',' +
                        ship.Ifcs.GoalStopTimes.Angular.Value.Z + ',' +

                        //Angular Velocities
                        ship.Ifcs.Tuning.AngularVelocity.X + ',' +
                        ship.Ifcs.Tuning.AngularVelocity.Y + ',' +
                        ship.Ifcs.Tuning.AngularVelocity.Z + ',' +

                        //Angular thrust toals
                        ship.Ifcs.Tuned.AngularThrustTotal().Pos.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Pos.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Pos.Z.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Neg.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Neg.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Neg.Z.ToString("F") + ',' +

                        //Angular Jerk Values
                        ship.Ifcs.Tuned.AngularJerk.Value.X + ',' +
                        ship.Ifcs.Tuned.AngularJerk.Value.Y + ',' +
                        ship.Ifcs.Tuned.AngularJerk.Value.Z + ',' +

                        //Other
                        ship.Ifcs.Tuned.Boost + ',' +
                        ship.Ifcs.Tuned.ABBoost + ',' +
                        //Boosted Values

                        //Goal times
                        //"SCM-Goal-Pos-X,SCM-Goal-Pos-Y,SCM-Goal-Pos-Z," +
                        //"SCM-Goal-Neg-X,SCM-Goal-Neg-Y,SCM-Goal-Neg-Z," +
                        ship.GetBoostedLinearScmGoalTimes().Pos.X.ToString("F") + ',' +
                        ship.GetBoostedLinearScmGoalTimes().Pos.Y.ToString("F") + ',' +
                        ship.GetBoostedLinearScmGoalTimes().Pos.Z.ToString("F") + ',' +
                        ship.GetBoostedLinearScmGoalTimes().Pos.X.ToString("F") + ',' +
                        ship.GetBoostedLinearScmGoalTimes().Pos.Y.ToString("F") + ',' +
                        ship.GetBoostedLinearScmGoalTimes().Pos.Z.ToString("F") + ',' +

                        //Goal times
                        //"ABM-Goal-Pos-X,ABM-Goal-Pos-Y,ABM-Goal-Pos-Z," +
                        //"ABM-Goal-Neg-X,ABM-Goal-Neg-Y,ABM-Goal-Neg-Z," +
                        ship.GetBoostedLinearAbmGoalTimes().Pos.X.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmGoalTimes().Pos.Y.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmGoalTimes().Pos.Z.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmGoalTimes().Pos.X.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmGoalTimes().Pos.Y.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmGoalTimes().Pos.Z.ToString("F") + ',' +

                        //Goal distance
                        //"ABM-Dist-Pos-X,ABM-Dist-Pos-Y,ABM-Dist-Pos-Z," +
                        //"ABM-Dist-Neg-X,ABM-Dist-Neg-Y,ABM-Dist-Neg-Z," +
                        ship.GetBoostedLinearAbmStopDistance().Pos.X.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmStopDistance().Pos.Y.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmStopDistance().Pos.Z.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmStopDistance().Pos.X.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmStopDistance().Pos.Y.ToString("F") + ',' +
                        ship.GetBoostedLinearAbmStopDistance().Pos.Z.ToString("F") + ',' +

                        //Boosted aggregate
                        (ship.Ifcs.Tuned.AggregateThrust*ship.Ifcs.Tuned.Boost).ToString("F") + "," +

                        //Linear accelerations totals
                        (ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.X*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Y*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Z*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.X*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Y*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Z*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        //Linear thrust totals
                        (ship.Ifcs.Tuned.LinearThrustTotal().Pos.X*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearThrustTotal().Pos.Y*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearThrustTotal().Pos.Z*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearThrustTotal().Neg.X*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearThrustTotal().Neg.Y*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearThrustTotal().Neg.Z*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        //Linear jerkIn values
                        (ship.Ifcs.Tuned.LinearJerk.Pos.X*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearJerk.Pos.Y*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearJerk.Pos.Z*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearJerk.Neg.X*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearJerk.Neg.Y*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        (ship.Ifcs.Tuned.LinearJerk.Neg.Z*ship.Ifcs.Tuned.Boost).ToString("F") + ',' +
                        ship.Ifcs.Tuned.RotationScale;
                    fileOut.Add(s);
                }

                StreamWriter fifoStreamWriter =
                    new StreamWriter(
                        IfcsGenerator.outputPath + @"\" + IfcsGenerator.Build + "-" +
                        IfcsGenerator.IFCS_EXPORT_DATA + ".csv", false,
                        Encoding.UTF8);

                foreach (string line in fileOut)
                {
                    fifoStreamWriter.WriteLine(line);
                }

                fifoStreamWriter.Flush();
                fifoStreamWriter.Close();
            }
        }

        internal static void ExportSQL(Dictionary<string, Spaceship> ships)
        {
            List<String> fileOut = new List<string>();
            //"Velocity Goal", "Velocity@Goal", "Variance"
            fileOut.Add("LocalName,Shipname,Shipbase," +
                        //mass
                        "mass-hull,mass-items,mass-total,mass-tuned," +
                        //vCaps
                        "SCM,ABM,CM," +
                        //Goal times
                        "Linear-Goal-Pos-X,Linear-Goal-Pos-Y,Linear-Goal-Pos-Z," +
                        "Linear-Goal-Neg-X,Linear-Goal-Neg-Y,Linear-Goal-Neg-Z," +
                        //Linear thrust acceleration
                        "Linear-Acceleration-Pos-X,Linear-Acceleration-Pos-Y,Linear-Acceleration-Pos-Z," +
                        "Linear-Acceleration-Neg-X,Linear-Acceleration-Neg-Y,Linear-Acceleration-Neg-Z," +
                        //Linear thrust values
                        "Linear-Thrust-Pos-X,Linear-Thrust-Pos-Y,Linear-Thrust-Pos-Z," +
                        "Linear-Thrust-Neg-X,Linear-Thrust-Neg-Y,Linear-Thrust-Neg-Z," +
                        //Linear jerkIn
                        "Linear-jerkIn-Pos-X,Linear-jerkIn-Pos-Y,Linear-jerkIn-Pos-Z," +
                        "Linear-jerkIn-Neg-X,Linear-jerkIn-Neg-Y,Linear-jerkIn-Neg-Z," +
                        //Angular Goal times
                        "Angular-Goal-X,Angular-Goal-Y,Angular-Goal-Z," +
                        //Angular velocity
                        "Angular-V-X,Angular-V-Y,Angular-V-Z," +
                        //Angular thrust values
                        "Angular-Thrust-Pos-X,Angular-Thrust-Pos-Y,Angular-Thrust-Pos-Z," +
                        "Angular-Thrust-Neg-X,Angular-Thrust-Neg-Y,Angular-Thrust-Neg-Z," +
                        //Angular Jerk
                        "Angular-jerkIn-X,Angular-jerkIn-Y,Angular-jerkIn-Z," +
                        //Other
                        "Boost," +
                        "AB-Boost," +
                        "Rotation Scale"
                );

            foreach (var item in ships)
            {
                var ship = item.Value;

                {
                    string s =
                        "insert into ifcs_" + IfcsGenerator.Build + " values (" +
                        //Name/modification meta
                        "'" + ship.Localname + "'" + ',' +
                        "'" + ship.Shipname + "'" + ',' +
                        "'" + ship.Shipbase + "'" + ',' +
                        //Ship mass
                        ship.Ifcs.Tuned.Mass.Hull + ',' +
                        ship.Ifcs.Tuned.Mass.Items + ',' +
                        ship.Ifcs.Tuned.Mass.Total + ',' +
                        ship.Ifcs.Tuned.Mass.TunedMass + ',' +
                        //Tuning speeds
                        ship.Ifcs.Tuning.ScmVelocity + ',' +
                        ship.Ifcs.Tuning.AbmVelocity + ',' +
                        ship.Ifcs.Tuning.CmVelocity + ',' +
                        //Goal stop times
                        ship.Ifcs.GoalStopTimes.Linear.Pos.X + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Pos.Y + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Pos.Z + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Neg.X + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Neg.Y + ',' +
                        ship.Ifcs.GoalStopTimes.Linear.Neg.Z + ',' +

                        //Linear accelerations totals
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Pos.Z.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearAccelerationTotal().Neg.Z.ToString("F") + ',' +
                        //Linear thrust totals
                        ship.Ifcs.Tuned.LinearThrustTotal().Pos.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Pos.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Pos.Z.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Neg.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Neg.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.LinearThrustTotal().Neg.Z.ToString("F") + ',' +
                        //Linear jerkIn values
                        ship.Ifcs.Tuned.LinearJerk.Pos.X + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Pos.Y + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Pos.Z + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Neg.X + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Neg.Y + ',' +
                        ship.Ifcs.Tuned.LinearJerk.Neg.Z + ',' +

                        //Angular Goals
                        ship.Ifcs.GoalStopTimes.Angular.Value.X + ',' +
                        ship.Ifcs.GoalStopTimes.Angular.Value.Y + ',' +
                        ship.Ifcs.GoalStopTimes.Angular.Value.Z + ',' +

                        //Angular Velocities
                        ship.Ifcs.Tuning.AngularVelocity.X + ',' +
                        ship.Ifcs.Tuning.AngularVelocity.Y + ',' +
                        ship.Ifcs.Tuning.AngularVelocity.Z + ',' +

                        //Angular thrust toals
                        ship.Ifcs.Tuned.AngularThrustTotal().Pos.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Pos.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Pos.Z.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Neg.X.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Neg.Y.ToString("F") + ',' +
                        ship.Ifcs.Tuned.AngularThrustTotal().Neg.Z.ToString("F") + ',' +

                        //Angular Jerk Values
                        ship.Ifcs.Tuned.AngularJerk.Value.X + ',' +
                        ship.Ifcs.Tuned.AngularJerk.Value.Y + ',' +
                        ship.Ifcs.Tuned.AngularJerk.Value.Z + ',' +

                        //Other
                        ship.Ifcs.Tuned.Boost + ',' +
                        ship.Ifcs.Tuned.ABBoost + ',' +
                        ship.Ifcs.Tuned.RotationScale + ");";
                    fileOut.Add(s);
                }

                StreamWriter fifoStreamWriter =
                    new StreamWriter(
                        IfcsGenerator.outputPath + @"\" + IfcsGenerator.Build + "-" +
                        IfcsGenerator.IFCS_EXPORT_DATA + ".sql", false,
                        Encoding.UTF8);

                foreach (string line in fileOut)
                {
                    fifoStreamWriter.WriteLine(line);
                }

                fifoStreamWriter.Flush();
                fifoStreamWriter.Close();
            }
        }


        internal static void GetTunedMass(Dictionary<string, Spaceship> ships)
        {
            foreach (KeyValuePair<string, Spaceship> key in ships)
            {
                Spaceship ship = key.Value;
                float hullMass = ship.Ifcs.Tuned.Mass.Hull;
                float baseMass = hullMass/1.5f;
                float baseThrust = ship.Ifcs.Tuned.LinearThrustTotal().Pos.Y;
                float baseGoal = ship.Ifcs.GoalStopTimes.Linear.Neg.Y;
                float baseJerk = ship.Ifcs.Tuned.LinearJerk.Pos.Y;
                float baseAcceleration = baseThrust/baseMass;
                float baseVelocity = ship.Ifcs.Tuning.ScmVelocity;

                bool isTuned = false;
                bool VGoalMet = false;
                bool AGoalMet = false;
                bool TGoalMet = false;

                int iterations = 600000;
                int counter = 0;
                int resolution = 300;
                List<MotionData> motion = new List<MotionData>();

                do
                {
                    baseMass += 10;
                    baseAcceleration = baseThrust/baseMass;
                    counter++;

                    GetMassFromAxisMotion(resolution, baseAcceleration, baseJerk, motion, baseGoal, baseVelocity);
                    //check values
                    var motionData = motion.Last();
                    //Console.WriteLine(motionData.Velocity);


                    if (isWithinTolerance(baseVelocity, motionData.Velocity, 1f) && motionData.Jerk < 0 &&
                        motionData.Acceleration < 1f)
                    {
                        break;
                    }
                    else
                    {
                        motion.Clear();
                    }


                    if (IfcsGenerator.isLastMassOutputOn && counter%10 == 0)
                    {
                        ;
                        Console.WriteLine("------------------------------------");
                        Console.WriteLine("Time: " + motionData.Time);
                        Console.WriteLine("Time Goal: " + baseGoal);
                        Console.WriteLine("Jerk: " + motionData.Jerk);
                        Console.WriteLine("Acceleration: " + motionData.Acceleration);
                        Console.WriteLine("Goal Acceleration: " + baseAcceleration);
                        Console.WriteLine("Velocity: " + motionData.Velocity);
                        Console.WriteLine("Goal Velocity: " + baseVelocity);
                        Console.WriteLine("Distance: " + motionData.Distance);
                        Console.WriteLine("MaxV: " + baseVelocity);
                        Console.WriteLine("Mass: " + baseMass);
                    }
                } while (counter < iterations);


                ship.Ifcs.Tuned.Mass.TunedMass = baseMass;
            }
        }

        private static bool isWithinTolerance(float baseVelocity, double velocity, float tolerance)
        {
            if (velocity < (baseVelocity + tolerance) && velocity > baseVelocity - tolerance)
            {
                return true;
            }
            return false;
        }

        internal static void GetMassFromAxisMotion(int resolution, float acceleration, float jerk,
            List<MotionData> motion,
            float timeGoal,
            float scmVelocity)
        {
            double interval = 1f/resolution;

            double jerkTime = acceleration/jerk;
            Boolean isP1Complete = false;
            double time = interval;

            //T-zero
            motion.Add(new MotionData(0, 0, 0, 0, 0));


            //sample.IFCS.Tuning.SCMVelocity
            //while velocity less than max
            do
            {
                //Phase 1: Acceleration build up
                if (motion.Last().Acceleration < acceleration && !isP1Complete)
                {
                    double a = motion.Last().Acceleration + jerk*interval;
                    double v = motion.Last().Velocity + a*interval + 0.5f*jerk*interval*interval;
                    double d = motion.Last().Distance
                               + v*interval
                               + 0.5f*a*interval*interval
                               + (1/6f)*jerk*interval*interval*interval;

                    if (a >= acceleration)
                    {
                        isP1Complete = true;
                    }

                    motion.Add(new MotionData(time, d, v, a, jerk));
                }
                //phase 2: Limit acceleration
                else if (time < timeGoal - jerkTime)
                {
                    double a = acceleration;
                    double v = motion.Last().Velocity + a*interval;
                    double d = motion.Last().Distance
                               + v*interval
                               + 0.5f*a*interval*interval
                               + (1/6f)*jerk*interval*interval*interval;
                    motion.Add(new MotionData(time, d, v, a, 0));
                }
                else
                {
                    double a = motion.Last().Acceleration - jerk*interval;
                    double v = motion.Last().Velocity + a*interval + 0.5f*jerk*interval*interval;
                    double d = motion.Last().Distance
                               + v*interval
                               + 0.5f*a*interval*interval
                               + (1/6f)*jerk*interval*interval*interval;

                    motion.Add(new MotionData(time, d, v, a, -jerk));
                }

                if (IfcsGenerator.IsOutputOn)
                {
                    Console.WriteLine("------------------------------------");
                    Console.WriteLine("Time: " + time);
                    Console.WriteLine("Jerk: " + motion.Last().Jerk);
                    Console.WriteLine("Acceleration: " + motion.Last().Acceleration);
                    Console.WriteLine("Velocity: " + motion.Last().Velocity);
                    Console.WriteLine("Distance: " + motion.Last().Distance);
                    Console.WriteLine("MaxV: " + scmVelocity);
                }


                Math.Round(time += interval, 2);
            } while (motion.Last().Velocity < scmVelocity && motion.Last().Time < timeGoal);
        }
    }
}